from abc import ABC, abstractmethod
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Dict, Any

class BaseModel(ABC):
    """Models the data of a BaseGraphicsItem.
    
    This abstract base class is not intended to be instantiated. It is intended
    to be inherited by a concrete subclass.
    """
    def __init__(self) -> None:
        pass

    def save(self, property: 'Dict[str, str]', value: str, attribute: str = 'value') -> None:
        """Set attribute in property dictionary to value.

        Args:
            property: Property whose attribute to be set.
            value: Value to set attribute to.
            attribute: Attribute to be set to value.
        """
        property[attribute] = value

    @classmethod
    @abstractmethod
    def from_dict(cls, in_dict: 'Dict[str, Any]') -> 'BaseModel':
        """Creates a BaseGraphicsItem from a dictionary representation.

        This method must be implemented by classes inheriting this abstract
        base class.

        Args:
            in_dict: The dictionary representation of a BaseGraphicsItem.

        Returns:
            A BaseGraphicsItem based on the in_dict arg.
        """
        pass

    @abstractmethod
    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the BaseGraphicsItem to a dictionary representation.

        This method must be implemented by classes inheriting this abstract
        base class.

        Returns:
            A dictionary representation of the BaseGraphicsItem.
        """
        pass
