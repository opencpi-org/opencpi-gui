from pathlib import Path
import re
import sys
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Dict, List, Any, Optional, Union
    import xml.etree.ElementTree as ET
    import xml.dom.minidom as MD

from PyQt5.QtWidgets import QGraphicsItem, QGraphicsProxyWidget, QLabel
from PyQt5.QtGui import QPen, QColor, QBrush, QPainterPath
from PyQt5.QtCore import Qt, QPoint, QRectF, QPointF
if TYPE_CHECKING:
    from PyQt5.QtGui import QPainter
    from PyQt5.QtWidgets import QWidget, QStyleOptionGraphicsItem

path = Path(__file__).parents[2]
sys.path.append(str(path))
from .model import NodeModel
from .properties_widget import NodePropertiesWidget
from base_classes import BaseGraphicsItem
from connection import Line
from OpenCPI_GUI import ocpidev_show
from port import Port
if TYPE_CHECKING:
    from node_editor.scene import Scene

class Node(BaseGraphicsItem):
    """Graphical representation of a component."""
    _flags = (
        QGraphicsItem.ItemIsMovable 
        | QGraphicsItem.ItemIsSelectable 
        | QGraphicsItem.ItemSendsGeometryChanges
    )
    _snap_range = 1000
    # from os import environ
    # opencpi = environ['OCPI_ROOT_DIR']
    # node_dict = {
    #     'ocpi.core.backpressure': {
    #         'directory': f'{opencpi}/projects/core/components/specs/backpressure-spec.xml',
    #         'package_id': 'ocpi.core'
    #     },
    #     'ocpi.core.gp-in': {
    #         'directory': f'{opencpi}/projects/core/specs/gp_in-spec.xml',
    #         'package_id': 'ocpi.core'
    #     },
    #     'ocpi.core.gp-out': {
    #         'directory': f'{opencpi}/projects/core/specs/gp_out-spec.xml',
    #         'package_id': 'ocpi.core'
    #     },
    #     'ocpi.assets.util_comps.advanced_pattern': {
    #         'directory': f'{opencpi}/projects/assets/components/util_comps/specs/advanced_pattern-spec.xml',
    #         'package_id': 'ocpi.assets.util_comps'
    #     }
    # }
    node_dict = ocpidev_show(None, 'components', options={'--json': True})
    
    def __init__(
        self, 
        component: str, 
        path: Path, 
        package_id: str, 
        name: 'Optional[str]' = ..., 
        model: 'Optional[NodeModel]' = ...,
    ) -> None:
        super().__init__()
        if model is ... or model is None:
            model = NodeModel(component, path, package_id, name=name)
        self._model: NodeModel = model
        self._ports = []
        self._menu_actions = []
        self._properties_widget = None
        self._proxy_widget = QGraphicsProxyWidget(self)
        self._init_sizes()
        self._init_assets()
        self.name = self._model.name
        self._vertical_snap_line = None
        self._horizontal_snap_line = None
        
    @property
    def properties_widget(self) -> NodePropertiesWidget:
        """QWidget containing the component's properties."""
        if self._properties_widget: return self._properties_widget
        self._properties_widget = NodePropertiesWidget(self)
        return self._properties_widget

    @property
    def component(self) -> str:
        """The name of component that the Node represents."""
        return self._model.component

    @component.setter
    def component(self, component: str) -> None:
        self._model.component = component

    @property
    def name(self) -> str:
        """The name of the instance of the Node's component."""
        return self._model.name

    @name.setter
    def name(self, name: str) -> None:
        if self.scene and name in self.scene.nodes_dict:
            name = self.name
        elif self.scene and self.name in self.scene.nodes_dict:
            if self.scene.nodes_dict[self.name] is self:
                self.scene.nodes_dict.pop(self.name)
        self._model.name = name
        if self.scene:
            self.scene.nodes_dict[self.name] = self
        self.properties_widget.name_line.setText(self._model.name)
        label_text = self.name[:16] + '...' if len(self.name) > 17 else self.name
        self.name_label = QLabel(label_text)
        self.name_label.setStyleSheet('background-color: rgba(0, 0, 0, 0); color: white')
        self.setToolTip(self.name)
        self._proxy_widget.setWidget(self.name_label)
        pos = self.boundingRect().center() - self.name_label.rect().center() #type: ignore
        self._proxy_widget.setPos(pos)

    @property
    def package_id(self) -> str:
        """The package ID of the Node's component."""
        return self._model.package_id

    @package_id.setter
    def package_id(self, package_id: str) -> None:
        self._model.package_id = package_id

    @property
    def properties(self) -> 'Optional[Dict[str, Any]]':
        """Dictionary containing the Node's component's properties."""
        return self._model.properties

    @property
    def path(self) -> Path:
        """The file system Path where the Node's component lives."""
        return self._model.path

    @path.setter
    def path(self, path: Path) -> None:
        self._model.path = path

    @property
    def ports(self) -> 'List[Port]':
        """List of Ports belonging to the Node's component."""
        if self._ports: return self._ports
        y_left = y_right = self.height // 2 - 25
        if self._model.ports is None:
            return []
        for name, props in self._model.ports.items():
        # Construct the Ports from the model
            port = Port(name, props, node=self)
            x = int(self.pos().x())
            if port._model.is_producer:
            # Producer Ports on the right side of Node
                y_right += 25
                x += self.width
                pos = QPoint(x, y_right)  
            else:
            # Consumer Ports on the left side of Node
                y_left += 25
                pos = QPoint(x, y_left)
            port.setPos(pos)
            self._ports.append(port)
        return self._ports

    @ports.setter
    def ports(self, ports: 'List[Port]') -> None:
        self._ports = ports

    @property
    def consumer_ports(self):
        """List of consumer Ports belonging to the Node's component."""
        consumer_ports = []
        for port in self.ports:
            if port.is_consumer:
                consumer_ports.append(port)
        return consumer_ports
    
    @property
    def producer_ports(self):
        """List of producer Ports belonging to the Node's component."""
        producer_ports = []
        for port in self.ports:
            if port.is_producer:
                producer_ports.append(port)
        return producer_ports

    @property
    def scene(self) -> 'Scene':
        """The Scene the Node belongs to."""
        return super().scene() #type: ignore

    def get_port(self, port_name: str) -> 'Optional[Port]':
        """Returns the Node's Port matching the port_name arg.
        
        Args:
            port_name: The name of the Port to return.
        
        Returns:
            The Port matching the port_name arg if it exists; otherwise None.
        """
        for port in self.ports:
            if port.name == port_name:
                return port
        return None

    def copy(self) -> 'Node':
        """Returns a copy of the Node.
        
        Gets a dictionary representation of the Node by calling the to_dict() 
        method, then passes the dictionary to the from_dict() method to make 
        the copy.
        """
        copy = self.from_dict(self.to_dict())
        return copy

    def delete(self) -> None:
        """Removes Node from its Scene.
        
        Also calls the Node's delete_connections() method to delete all
        Connections the Node's Ports are a part of.
        """
        self.scene.remove_node(self)
        self.delete_connections()
    
    def delete_connections(self) -> None:
        """Deletes all Connections that the Node's Ports are a part of.
        
        Iterates through each Port belonging to the Node and calls each Port's
        delete_connections() method.
        """
        for port in self.ports:
            if port.connection:
                port.connection.delete()

    def update_connections(self) -> None:
        """Updates all Connections that the Node's Ports are a part of.
        
        Iterates through each Port belonging to the Node and calls each Port's
        update_connections() method.
        """
        for port in self.ports:
            if port.connection:
                port.connection.update()

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the Node to a dictionary representation.

        Calls _model.to_dict() to get a dictionary representation of the
        NodeModel.

        Returns:
            A Dictionary representation of the Node.
        """
        out_dict = {
            'model': self._model.to_dict(),
            'ports': [port.to_dict() for port in self.ports],
            'pos': (self.pos().x(), self.pos().y())
        }
        return out_dict

    def to_xml(self, root: 'MD.Document') -> 'MD.Element':
        """Converts the Node to an xml minidom Element.

        Args:
            root: The minidom Document to add the Node to.

        Returns:
            An xml minidom Element representation of the Connection.
        """
        model_dict = self._model.to_dict()
        element = root.createElement('Instance')
        element.setAttribute('Component', model_dict['component'])
        element.setAttribute('Name', model_dict['name'])
        for name, attributes_dict in model_dict['properties'].items():
            child = root.createElement('Property')
            child.setAttribute('Name', name)
            value = attributes_dict.get('value', '')
            if attributes_dict.get('valuefile', False) and value:
            # ValueFile is a bool; actual value is still set in value
                child.setAttribute('Valuefile', value)
            else:
                value_string = self._member_to_str(attributes_dict)
                if not value_string or '{}' in value_string:
                    continue
                child.setAttribute("Value", value_string)
            for attribute_name in ['delay', 'dumpfile']:
                attribute = attributes_dict.pop(attribute_name, None)
                if attribute:
                    child.setAttribute(attribute_name.capitalize(), attribute)
            element.appendChild(child)
        return element

    def _member_to_str(self, members_dict: 'Dict[str, Any]'):
        """Helper method for to_xml() method to convert a member to a str.
        
        Calls itself recursively if the member contains members.

        Returns:
            A string representation of a member.
        """
        members = members_dict.get('members', None)
        value = members_dict.get('value', None)
        if members:
            member_strings = []
            for member_name, member in members.items():
                member_value = self._member_to_str(member)
                if member_value and member_value != '{}':
                    member_strings.append(f'{member_name} {member_value}')  
            member_string = '{' + ', '.join(member_strings)
            member_string += '}'
        elif value:
            member_string = value
        else:
            member_string = ''
        return member_string

    @classmethod
    def from_dict(cls, in_dict: 'Dict[str, Any]') -> 'Node':
        """Creates a Node from a dictionary.

        Args:
            in_dict: The dictionary representation of a Node.

        Returns:
            A Node based on the in_dict arg.
        """
        model = NodeModel.from_dict(in_dict['model'])
        node = cls(model.component, model.path, model.package_id, model=model, name=model.name)
        node.ports = [Port.from_dict(port, node=node) for port in in_dict['ports']]
        node.setPos(QPointF(*in_dict['pos']))
        return node

    @classmethod
    def from_xml(cls, in_xml: 'ET.Element', package_id: 'Optional[str]' = None) -> 'Node':
        """Creates a Nodefrom an xml minidom Element.

        Args:
            in_xml: The xml minidom Element representation of a Node.
            package_id: The package ID of the component which to create the
                Node for.

        Returns:
            A Node based on the in_xml arg.
        """
        in_xml.attrib = dict((k.lower(), v) for k,v in in_xml.attrib.items())
        bad_attribs = [x for x in in_xml.attrib if x not in ['name', 'component', 'connect']]
        if bad_attribs:
            raise Exception(f"Unsupported attributes: {', '.join(bad_attribs)}")
        if package_id:
            component = '.'.join([package_id, in_xml.attrib['component']])
        else:
            component = in_xml.attrib['component']
        if component not in Node.node_dict:
            raise Exception(f'Unknown component: {component}')
        node_model_dict = {
            'component': component,
            'package_id': Node.node_dict[component]['package_id'],
            'path': Path(Node.node_dict[component]['directory'])
        }
        if 'name' in in_xml.attrib:
            node_model_dict['name'] = in_xml.attrib['name']
        else:
            node_model_dict['name'] = component.split('.')[-1]
        valid_attribs = ['value', 'name', 'delay', 'dumpfile', 'valuefile']
        properties_dict = {}
        for child in in_xml.getchildren():
            child.attrib = dict((k.lower(), v) for k,v in child.attrib.items())
            bad_attribs = [x for x in child.attrib if x not in valid_attribs]
            if bad_attribs:
                raise Exception(f"Unsupported attributes: {', '.join(bad_attribs)}")
            name = child.attrib.pop('name')
            value = child.attrib.pop('value', None)
            if value:
                properties_dict[name] = cls._parse_property_str(value)
            else:
                properties_dict[name] = {}
            for prop_name in child.attrib:
                properties_dict[name][prop_name] = child.attrib[prop_name]
        model = NodeModel.from_dict(node_model_dict)
        model.update_properties(properties_dict)
        node = cls(model.component, model.path, model.package_id, model=model, name=model.name)
        return node

    @classmethod
    def _parse_property_str(cls, property_str: str) -> 'Dict[str, Any]':
        """Helper method for from_xml() method to parse a property string.
        
        Converts a string representation of the property to a dictionary.
        Calls itself recursively if the property value is a struct.

        Args:
            properties_str:

        Returns:
            A dictionary representation of a propertys.
        """
        properties_dict = {}
        if property_str.startswith('{') and property_str.endswith('}'):
        # Parse structs
            value_pattern = re.compile(r'(\w+)\s+({[^}]+}|\w+)')
            matches = value_pattern.findall(property_str)
            properties_dict['members'] = {}
            for name, value in matches:
                if value.startswith('{') and value.endswith('}'):
                # Nested struct; so recurse through struct
                    properties_dict['members'][name] = cls._parse_property_str(value)
                else:
                # Not nested struct; so assign to property
                    properties_dict['members'][name] = {'value': value}
        else:
        # Not a struct; just a regular value
            properties_dict['value'] = property_str
        return properties_dict

    def _init_sizes(self) -> None:
        """Initiates graphical dimensions of the Port."""
        self.width = 160
        self.height = 50
        self.height += 25 * (max(len(self.consumer_ports), len(self.producer_ports)) - 1)
        self.edge_roundness = 10.0
        self.edge_padding = 10.0
        self.title_height = 24.0
        self.title_horizontal_padding = 4.0
        self.title_vertical_padding = 4.0

    def _init_assets(self) -> None:
        """Initiates various graphical assets of the Port."""
        self._color = QColor("#7F000000")
        self._color_selected = QColor("#0FFF50")
        self._colore_highlight = QColor("#FFFFFF")
        self._pen_default = QPen(self._color)
        self._pen_default.setWidthF(2.0)
        self._pen_selected = QPen(self._color_selected)
        self._pen_selected.setWidthF(2.0)
        self._pen_highlight = QPen(self._colore_highlight)
        self._pen_highlight.setWidthF(3.0)
        self._brush_title = QBrush(QColor("#FF313131"))
        self._brush_background = QBrush(QColor("#E3212121"))

    def itemChange(self, change: 'QGraphicsItem.GraphicsItemChange', value: str) -> 'Any':
        """Overrides super method to update connections if position is changed.
        
        If the change was an ItemPositionChange, calls update_connections() 
        method to reflect the change in position graphically for the Node's 
        connections. Also calls super method.

        Args:
            change: The change that occured. If a change in position, Node's 
                connections are updated. Also required for call to super.
            value: Required for call to super.
        """
        if change == QGraphicsItem.ItemPositionChange:
            self.update_connections()
        return super().itemChange(change, value)
    
    def setPos(self, pos: 'Union[QPointF, QPoint]') -> None:
        """Overrides super method to set position of self to pos,
        
        Super normally sets position to upper left corner of item,
        so offset position by width and height of self to center setting
        position.

        Args:
            pos: The position to set Node to.
        """
        pos.setX(int(pos.x()) - self.width // 2)
        pos.setY(int(pos.y()) - self.height // 2)
        super().setPos(pos)
        self.update_connections()
    
    @property
    def center_pos(self):
        pos = self.pos()
        x = int(pos.x()) + self.width // 2
        y = int(pos.y()) + self.height // 2
        return QPointF(x, y)

    def boundingRect(self) -> 'QRectF':
        """Required to be implemented by QT for a QGraphicsItem.
        
        Sets graphical dimensions of the Node.
        """
        return QRectF(0, 0, self.width, self.height).normalized()

    def paint(
        self, 
        painter: 'QPainter', 
        option: 'QStyleOptionGraphicsItem', 
        widget: 'QWidget',
    ) -> None:
        """Required to be implemented by QT for a QGraphicsItem.
        
        Sets color of Node outline dependant on whether Node is currently 
        highlighted, selected, or neither.

        Args:
            painter: Required for call to super.
            option: Required for call to super.
            widget: Required for call to super.
        """
        painter.setPen(Qt.NoPen)
        painter.setBrush(self._brush_title)
        path_content = QPainterPath()
        path_content.setFillRule(Qt.WindingFill)
        path_content.addRoundedRect(0, 0, self.width, 
            self.height, self.edge_roundness, self.edge_roundness)
        painter.setPen(Qt.NoPen)
        painter.setBrush(self._brush_background)
        painter.drawPath(path_content.simplified())
        # outline
        path_outline = QPainterPath()
        path_outline.addRoundedRect(-1, -1, self.width+2, self.height+2, 
            self.edge_roundness, self.edge_roundness)
        painter.setBrush(Qt.NoBrush)
        if self.is_highlighted:
            painter.setPen(self._pen_highlight)
        else:
            painter.setPen(self._pen_default if not self.isSelected() else self._pen_selected)
        painter.drawPath(path_outline.simplified())
    
    def find_nearest_node(self, scan_rect: QRectF) -> 'Optional[Node]':
        """Finds the nearest Node within the scan_rect.

        Ignores currently selected Nodes.

        Args:
            scan_rect: QRectF to search within for the nearest Node.
        Returns:
            The Nearest Node from self within the scan_rect.
        """
        items = self.scene.items(scan_rect)
        nearest_node = None
        nearest = 10000000000
        for item in items:
            if not isinstance(item, Node) or item.isSelected():
                continue
            qpdist = item.center_pos - self.center_pos #type: ignore
            dist = qpdist.x() * qpdist.x() + qpdist.y() * qpdist.y()
            if dist > 0 and dist < nearest:
                nearest_node, nearest = item, dist
        return nearest_node
    
    def mouseMoveEvent(self, event) -> None:
        """Extends super method to enable snapping to other Nodes.

        Unless shift key is held, calls find_nearest_node() method to 
        search for nearest Nodes along x and y axis to enable snapping 
        nodes horizontally and vertically. Calls _set_snap_line() 
        method to show or hide the guiding lines when snapping.
        
        Args:
            event: The event that triggered this method.
        """
        super().mouseMoveEvent(event)
        if event.modifiers() == Qt.ShiftModifier:
        # Don't snap if shift is held
            xnearest_node = None
            ynearest_node = None
        else:
        # Find nearest nodes to snap to
            xscan_rect = QRectF(
                self.center_pos.x() - self._snap_range, 
                self.center_pos.y() - 25, 
                self._snap_range * 2, 
                50
            )
            yscan_rect = QRectF(
                self.center_pos.x() - 25, 
                self.center_pos.y() - self._snap_range,  
                50,
                self._snap_range * 2
            )
            xnearest_node = self.find_nearest_node(xscan_rect)
            ynearest_node = self.find_nearest_node(yscan_rect)
            pos = QPointF(
                ynearest_node.center_pos.x() if ynearest_node is not None else self.center_pos.x(),
                xnearest_node.center_pos.y() if xnearest_node is not None else self.center_pos.y()
            )
            for node in self.scene.selectedItems():
            # Snap all selected nodes, maintaining proper distances
                if not isinstance(node, Node) or node == self:
                    continue
                dist = node.pos() - self.pos() # type: ignore
                node.setPos(pos + dist)
            self.setPos(pos) # Snap node to nearest nodes on x and y axis
        allow_create = xnearest_node is not ynearest_node
        # Update lines to nearest nodes or delete lines if shift held
        self._set_snap_line('horizontal', xnearest_node, snap_line=self._horizontal_snap_line,
                            allow_create=allow_create)
        self._set_snap_line('vertical', ynearest_node, snap_line=self._vertical_snap_line,
                            allow_create=allow_create)
        event.accept()

    def _set_snap_line(self, orientation, node=None, snap_line=None, allow_create=True) -> None:
        """Updates, creates, or deletes a guiding line for snapping.
        
        If node arg is None and snap_line arg is not None, snap_line 
        will be removed from the scene.

        Args:
            orientation: Whether the line is "horizontal" or "Vertical".
            node: The Node to extend the line to.
            snap_line: Existing Line to update.
            allow_create: Whether to allow creating of the Line in the
                case that the snap_line arg is None.
        """
        if node is not None:
            if snap_line is not None:
            # Update current line to nearest node on appropriate axis
                snap_line.start_pos = self.center_pos
                snap_line.end_pos = node.center_pos
            elif allow_create:
            # Create snap line to nearest node on appropriate axis
                line = Line(self.center_pos, node.center_pos, color=Qt.yellow, style=Qt.DashDotLine)
                if orientation == 'horizontal':
                    self._horizontal_snap_line = line
                elif orientation == 'vertical':
                    self._vertical_snap_line = line
                self.scene.addItem(line)
        elif snap_line is not None:
        # Remove current snap line
            self.scene.removeItem(snap_line)
            if snap_line is self._horizontal_snap_line:
                self._horizontal_snap_line = None
            elif snap_line is self._vertical_snap_line:
                self._vertical_snap_line = None

    def mouseReleaseEvent(self, event) -> None:
        """Extends super methods to remove any snap lines.
        
        Args: 
            event: Event that triggered this method.
        """
        super().mouseReleaseEvent(event)
        if self._horizontal_snap_line is not None:
            self.scene.removeItem(self._horizontal_snap_line)
            self._horizontal_snap_line = None
        if self._vertical_snap_line is not None:
            self.scene.removeItem(self._vertical_snap_line)
            self._vertical_snap_line = None
