from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import List, Any, Dict

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QStyle, QLabel, QPushButton, QFileDialog, QComboBox, QLineEdit, QGroupBox, QVBoxLayout, 
    QHBoxLayout)

from base_classes import BasePropertiesWidget
if TYPE_CHECKING:
    from node import Node

class NodePropertiesWidget(BasePropertiesWidget):
    """Container widget for widgets related to a Node's properties."""
    def __init__(self, node: 'Node') -> None:
        self._node = node
        super().__init__()

    def _init_widgets(self) -> None:
        """Initializes the QWidgets for the Node's properties."""
        widgets = []
        label = QLabel('Component')
        label.setAlignment(Qt.AlignHCenter)
        widgets.append(label)
        
        # Group box for name, component, and package ID
        group_box = QGroupBox()
        layout = QVBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)

        # Name editable, sub widget within group box
        name_widget = QWidget()
        name_layout = QHBoxLayout()
        name_layout.setContentsMargins(0, 0, 0, 0)
        name_widget.setLayout(name_layout)
        name_label= QLabel(f'Name:')
        self.name_line = QLineEdit(f'{self._node.name}')
        self.name_line.editingFinished.connect(
            lambda: setattr(self._node, 'name', self.name_line.text()))
        name_layout.addWidget(name_label)
        name_layout.addWidget(self.name_line)
        layout.addWidget(name_widget)

        # Component and Package ID not editable
        component_label = QLabel(f'Component: {self._node.component.split(".")[-1]}')
        package_id_label = QLabel(f'Package ID: {self._node.package_id}')
        layout.addWidget(component_label)
        layout.addWidget(package_id_label)
        group_box.setLayout(layout)
        group_box.setAlignment(Qt.AlignHCenter)
        widgets.append(group_box)
        for property_widget in self._get_property_widgets():
            widgets.append(property_widget)
        self.addWidgets(widgets)

    def _get_property_widgets(self) -> 'List[QWidget]':
        """Returns QWidgets for the Node's properties.
        
        Helper method for _init_widgets() method. Calls _get_property_widget()
        to get a QWidget for each property.

        Returns:
            QWidgets for the Node's properties.
        """
        property_widgets = []
        if not self._node.properties:
            return property_widgets
        for key,val in self._node.properties.items():
            property_type = val.get('type', '').lower()
            if not property_type:
                continue
            title = f'{key} ({property_type})'
            layout = QVBoxLayout()
            group_box = QGroupBox(title)
            group_box.setLayout(layout)
            if 'description' in val.keys():
                description = "<FONT COLOR=black>"
                description += val['description']
                description += "</FONT>"
                group_box.setToolTip(description)
            widget = self._get_property_widget(val, property_type)
            layout.addWidget(widget)
            property_widgets.append(group_box)
        return property_widgets

    def _get_property_widget(
        self, 
        property: 'Dict[str, Any]', 
        property_type: str, 
        is_member: bool = False
    ) -> QWidget:
        """Creates a QWidget for a specific Node property.
        
        Helper method for _init_widgets() method. Calls _get_members_widget()
        to get QWidgets for properties that are structs. Calls
        _get_attribute_widgets() to get QWidgets for each attribute of a
        property.

        Args:
            property: Dictionary contianing a specific Node property.
            property_type: The type of the property arg.
            is_member: Whether the property is a member of a struct.
        Returns:
            QWidget for a the property arg.
        """
        container_widget = QWidget()
        container_layout = QVBoxLayout()
        container_widget.setLayout(container_layout)
        property_widget = QWidget()
        property_layout = QHBoxLayout()
        property_layout.setContentsMargins(1, 1, 1, 1)
        property_widget.setLayout(property_layout)

        # Add browse button for ability to search for valuefile
        browse_widget = QPushButton()
        browse_widget.setIcon(self.style().standardIcon(QStyle.SP_DirIcon))
        browse_widget.setToolTip("Browse for ValueFile")
        browse_widget.setDisabled(True)
        is_valueFile = bool(property.get('valuefile', False))
        value_widget = None

        if is_valueFile:
            value = property.get('value')
            browse_widget.setEnabled(True)
        elif 'value' in property:
            value = property.get('value', None)
        else:
            value = property.get('default', '')
        if property_type == 'struct':
            members_widget = self._get_members_widget(property['members'])
            members_widget.setEnabled(not is_valueFile)
        else:
            members_widget = None
        if not is_member and property_type != 'bool':
            value_widget = QComboBox()
            value_widget.addItem('value')
            value_widget.addItem('valueFile')
            value_widget.setCurrentText('valueFile' if is_valueFile else 'value')
            value_widget.currentTextChanged.connect(
                lambda value: self._node.save(property, value=='valueFile', attribute='valuefile'))
            value_widget.currentTextChanged.connect(
                lambda value: browse_widget.setEnabled(value=='valueFile')
            )
            if members_widget is not None:
                value_widget.currentTextChanged.connect(
                    lambda value: members_widget.setDisabled(value=='valueFile'))
            property_layout.addWidget(value_widget)
        elif property_type != 'struct':
            value_widget = QLabel('value:')
            property_layout.addWidget(value_widget)
        if property_type == 'bool':
            widget = QComboBox()
            widget.addItem('true')
            widget.addItem('false')
            value = value if value is not None else 'true'
            widget.setCurrentText(value)
            widget.currentTextChanged.connect(
                lambda value: self._node.save(property, value))
            property_layout.addWidget(widget)
        elif not is_member or property_type != 'struct':
            widget = QLineEdit()
            value = value if value is not None else ''
            widget.setText(value)
            widget.textChanged.connect(
                lambda value: self._node.save(property, value))
            property_layout.addWidget(widget)
            property_layout.addWidget(browse_widget)
            if not is_member and property_type == 'struct':
                widget.setEnabled(is_valueFile)
                if value_widget is not None:
                    value_widget.currentTextChanged.connect(
                        lambda value: widget.setDisabled(value=='value'))
        
        browse_widget.clicked.connect(lambda: self._browse_valuefile(property, widget))
        container_layout.addWidget(property_widget)
        for attribute_widget in self._get_attribute_widgets(property, is_member=is_member):
            container_layout.addWidget(attribute_widget)
        if members_widget is not None:
            container_layout.addWidget(members_widget)
        container_layout.setContentsMargins(1, 1, 1, 1)
        return container_widget

    def _get_attribute_widgets(
        self, 
        property: 'Dict[str, Any]', 
        is_member: bool = False,
    ) -> 'List[QWidget]':
        """Creates QWidgets for a specific Node property's attributes.
        
        Helper method for _init_widgets() method. Calls _get_attribute_widget()
        to get a QWidget for each property attribute.

        Args:
            property: Dictionary contianing a specific Node property.
            is_member: Whether the property is a member of a struct.
        Returns:
            QWidgets for each attribute in the property arg.
        """
        attribute_widgets = []
        for attribute in property:
            if attribute in ['value', 'type', 'default', 'description', 'valueFile', 'members']:
                continue
            if attribute in ['delay', 'dumpfile'] and is_member:
                continue
            attribute_widget = self._get_attribute_widget(property, attribute)
            attribute_widgets.append(attribute_widget)
        return attribute_widgets

    def _get_attribute_widget(self, property: 'Dict[str, Any]', attribute: str) -> QWidget:
        """Creates a QWidget for a specific Node property's attribute.
        
        Helper method for _init_widgets() method.

        Args:
            property: Dictionary contianing a specific Node property.
            attribute: String value of the attribute to create a QWidget for.
        Returns:
            QWidget for a specific attribute in the property arg.
        """
        value = property.get(attribute, '')
        if attribute in ['delay', 'dumpfile']:
            layout = QHBoxLayout()
            attribute_widget = QWidget()
            label_widget = QLabel(f'{attribute}:')
            line_edit_widget = QLineEdit()
            line_edit_widget.setText(value)
            func = lambda value: self._node.save(property, value, attribute=attribute)
            line_edit_widget.textChanged.connect(func)
            layout.addWidget(label_widget)
            layout.addWidget(line_edit_widget)
            layout.setContentsMargins(1, 1, 1, 1)
            attribute_widget.setLayout(layout)
        else:
            layout = QHBoxLayout()
            attribute_widget = QWidget()
            label_widget = QLabel(f'{attribute}: {value}')
            label_widget.setWordWrap(True)
            layout.setContentsMargins(1, 1, 1, 1)
            layout.addWidget(label_widget)
            attribute_widget.setLayout(layout)
        return attribute_widget

    def _get_members_widget(self, members_dict: 'Dict[str, Any]') -> QWidget:
        """Creates a QWidget for a specific Node property struct.
        
        Helper method for _init_widgets() method. Calls _get_property_widget()
        method to create a QWidget for each individual member of the struct.

        Args:
            members_dict: Dictionary of the struct property.
        Returns:
            QWidget for a specific Node property struct.
        """
        members_widget = QWidget()
        members_layout = QVBoxLayout()
        members_layout.setContentsMargins(1, 1, 1, 1)
        members_widget.setLayout(members_layout)
        for name, member in members_dict.items():
            property_type = member['type'].lower()
            if property_type is None:
                continue
            member_widget = QGroupBox(f'{name} ({property_type})')
            member_layout = QVBoxLayout()
            member_layout.setContentsMargins(1, 1, 1, 1)
            member_widget.setLayout(member_layout)
            property_widget = self._get_property_widget(member, property_type, is_member=True)
            member_layout.addWidget(property_widget)
            members_layout.addWidget(member_widget)
        return members_widget
        
    def _browse_valuefile(self, property: 'Dict[str, Any]', text_widget: QLineEdit) -> None:
        """Launches browse window for ValueFile selection.
        
        Function called when ValueFile Button is clicked, launches browse window, 
        updates text based on file selection, and sets ValueFile parameter.

        Args:
            property: dictionary of properties that browse button is associated with.
            text_widget: QLineEdit text box object that is associated with value / valueFile

        Returns:
            None
        """

        fname, _ = QFileDialog.getOpenFileName(self, "Select ValueFile", "")
        # setText handles changing the value parameter to the browsed file path and name
        text_widget.setText(fname)
        self._node.save(property, 'True', attribute='valuefile')       