from copy import deepcopy
from pathlib import Path
from typing import TYPE_CHECKING
import xml.etree.ElementTree as ET
if TYPE_CHECKING:
    from typing import Dict, Tuple, Any, Optional

from base_classes import BaseModel

class NodeModel(BaseModel):
    """Models the data of a Node.

    Attributes:
        name: The name of the instance of the Node's component.
        component: The name of the Node's component.
        path: The file system Path where the Node's component lives.
        package_id: The package ID of the Node's component.
        properties: Dictionary containing the Node's component's properties.
        ports: Dictionary containing the Node's ports.
    """
    def __init__(self, 
        component: str, 
        path: Path, 
        package_id: str, 
        properties: 'Optional[Dict[str, Any]]' = ..., 
        ports: 'Optional[Dict[str, Any]]' = ..., 
        name: 'Optional[str]' = ...,
    ) -> None:
        super().__init__()
        self.component = component
        if name is None or name is ...:
            name = component.split('.')[-1]
        self.name = name
        self.path = path
        self.package_id = package_id
        if properties is None or ports is None or properties is ... or ports is ...:
            parsed_properties, parsed_ports = self._parse_spec()
            if properties is None or properties is ...:
                properties = parsed_properties
            if ports is None or ports is ...:
                ports = parsed_ports
        self.properties = properties
        self.ports = ports

    def _parse_spec(
        self, 
        root: 'Optional[ET.Element]' = ...,
    ) -> 'Tuple[Optional[Dict[str, Any]], Optional[Dict[str, Any]]]':
        """Parses the Node's component spec to get properties and ports.
        
        If a root arg is not passed, opens the component spec located at the 
        Node's path attribute and parses it for the component's properties and 
        ports. Calls itself recursively, passing in the current root Element,
        whenever a struct property is encountered.

        Args:
            root: xml ElementTree Element to parse.
        
        Returns:
            Tuple containing two elements: 
                either a dictionary of properties or None
                and either a dictionary of ports or None.
        """
        properties = {}
        ports = {}
        if root is ... or root is None:
            tree = ET.parse(self.path)
            root = tree.getroot()
        if root is None:
            return (None, None)
        for child in root.getchildren():
            tag = child.tag.lower()
            if tag not in ['property', 'member', 'port', 'datainterfacespec']:
                continue
            attributes = dict((k.lower(), v) for k,v in child.attrib.items())
            name = attributes.pop('name')
            if tag in ['property', 'member']:
                properties[name] = attributes
                if 'type' not in attributes:
                    properties[name]['type'] = 'uLong'
                if attributes['type'].lower() == 'struct':
                    properties[name]['members'], _ = self._parse_spec(root=child)
                else:
                    properties[name]['value'] = attributes.get('default', '')
                properties[name]['dumpfile'] = attributes.get('dumpFile', '')
                properties[name]['delay'] = attributes.get('delay', '')
            else:
                ports[name] = attributes
        return properties, ports 

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the NodeModel to a dictionary representation.

        Returns:
            A dictionary containing attributes of the NodeModel.
        """
        out_dict = {
            'component': self.component,
            'name': self.name,
            'path': str(self.path),
            'package_id': self.package_id,
            'properties': deepcopy(self.properties),
            'ports': self.ports
        }
        return out_dict

    @classmethod
    def from_dict(cls, in_dict: 'Dict[str, Any]') -> 'NodeModel':
        """Creates a NodeModel from a dictionary.

        Args:
            in_dict: The dictionary representation of a NodeModel.

        Returns:
            A NodeModel based on the in_dict arg.
        """
        component = in_dict['component']
        name = in_dict.get('name', None)
        path = Path(in_dict['path'])
        package_id = in_dict['package_id']
        ports = in_dict.get('ports', None)
        properties = in_dict.get('properties', None)
        model = cls(component, path, package_id, ports=ports, properties=properties, name=name)
        return model

    def update_properties(
        self, 
        in_dict: 'Dict[str, Any]', 
        property_dict: 'Optional[Dict[str, Any]]' = ...
    ) -> None:
        """Updates the property dict arg with elements in the in_dict arg.
        
        If a property_dict arg is not passed, the property_dict arg will be
        set to the NodeModel's properties attribute. Will call itself
        recursively whenever a member property is encountered, passing in the
        property as the property_dict arg.

        Args:
            in_dict: Dictionary whose elements will be used to update values of
                the property_dict.
            property_dict: Dictionary whose values are to be updated with the
                in_dict.
        """
        if property_dict is None or property_dict is ...:
            property_dict = self.properties
        if property_dict is None:
            return
        property_dict = dict((k.lower(), v) for k,v in property_dict.items())
        in_dict = dict((k.lower(), v) for k,v in in_dict.items())
        for property_name, property_val in in_dict.items():
            if isinstance(property_val, dict):
                members_dict = property_val.pop('members', None)
                if members_dict:
                    self.update_properties(members_dict, property_dict[property_name]['members'])
                for attr, val in property_val.items():
                    if attr == 'valuefile':
                        property_dict[property_name]['value'] = val
                        property_dict[property_name]['valuefile'] = True
                    else:
                        property_dict[property_name][attr] = val
            else:
                property_dict[property_name]['value'] = property_val
