"""Contains classes that represent an OpenCPI component.

Node: Graphical representation of a component.

Typical usage:
    Create a Node:
        node = Node(component_name, component_path, component_package_id)
    Create a Node from a dictionary representation:
        node = Node.from_dict(dictionary)
    Convert a Node to a dictionary representation:
        dictionary = node.to_dict()
"""

from .node import Node
