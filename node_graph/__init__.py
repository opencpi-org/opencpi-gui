"""A node graph editor used for creating and editing OpenCPI applications.

Typical usage:
    node_editor.run()
"""

from .main import run
