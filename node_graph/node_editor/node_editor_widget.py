from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Optional
    from pathlib import Path

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QSplitter, QHBoxLayout

from .display_widget import DisplayWidget
from .scene import Scene
from .view import View
if TYPE_CHECKING:
    from main_window import MainWindow

class NodeEditorWidget(QWidget):
    """QWidget the contains the node editor's View and DisplayWidget.
    
    Attributes:
        path: The file system Path of the application being displayed in the
            NodeEditorWidget.
        displayer_widget: The currently ative DisplayWidget to display in the
            NodeEditorWidget.
        view: The View to display in the NodeEditorWidget.
        scene: The Scene tied to the NodeEditorWidget's View.
    """
    def __init__(
        self, 
        window: 'Optional[MainWindow]' = None, 
        scene: Scene = ..., 
        path: 'Optional[Path]' = None
    ) -> None:
        super().__init__(window)
        if scene is ...:
            scene = Scene(self)
        self.path = path
        self.display_widget = DisplayWidget()
        self.scene = scene
        self.view = View(self.scene, self.display_widget)
        self._splitter = QSplitter(Qt.Horizontal)
        self._splitter.addWidget(self.display_widget)
        self._splitter.addWidget(self.view)
        self._splitter.setStretchFactor(1, 1)
        self._splitter.setStretchFactor(0, 0)
        self._splitter.setSizes([340])
        layout = QHBoxLayout()
        layout.addWidget(self._splitter)
        self.setLayout(layout)
        
    def export(self, out_path: 'Path') -> bool:
        """Exports the displayed application to the out_path arg.
        
        Calls the Scene's export() method, passing in the out_path arg.

        Args:
            out_path: File system Path to export application to.

        Returns:
            Whether the application was able to be exported.
        """
        self.scene.export(out_path)
        return True

    def save(self) -> bool:
        """Saves the displayed application to the path attribute.
        
        Calls the save_as() method, passing in the path attribute.

        Returns:
            Whether the application was able to be saved.
        """
        if self.path:
            self.save_as(self.path)
            return True
        return False

    def save_as(self, out_path: 'Path') -> bool:
        """Saves the displayed application to the out_path arg.
        
        Calls the Scene's save() method, passing in the out_path arg. Sets
        the window title to the stem of the out_path arg.

        Returns:
            Whether the application was able to be saved.
        """
        self.scene.save(out_path)
        self.path = out_path
        self.setWindowTitle(str(out_path.stem))
        return True

    @classmethod
    def open(cls, in_path: 'Path', window: 'MainWindow') -> 'NodeEditorWidget':
        """Opens an application from the in_path arg.
        
        Calls the Scene open() class method, passing in the in_path arg. Sets
        the window title to the stem of the in_path arg.

        Args:
            in_path: File system Path to the application to open.
            window: The MainWindow to open the application into.

        Returns:
            A NodeEditorWidget for the application located at the in_path arg.
        """
        scene = Scene.open(in_path)
        node_editor_widget = cls(window=window, scene=scene, path=in_path)
        node_editor_widget.setWindowTitle(str(in_path.stem))
        return node_editor_widget
    
    @classmethod
    def import_(cls, in_path: 'Path', window: 'MainWindow') -> 'Optional[NodeEditorWidget]':
        """Imports an application from the in_path arg.
        
        Calls the Scene import_() class method, passing in the in_path arg. 
        Sets the window title to the stem of the in_path arg.

        Args:
            in_path: File system Path to the application to import.
            window: The MainWindow to import the application into.

        Returns:
            A NodeEditorWidget for the application located at the in_path arg.
        """
        scene = Scene.import_(in_path)
        if scene:
            node_editor_widget = cls(window=window, scene=scene)
            node_editor_widget.setWindowTitle(str(in_path.stem))
        else:
            node_editor_widget = None
        return node_editor_widget
