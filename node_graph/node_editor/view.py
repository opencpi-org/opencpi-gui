from pathlib import Path
from time import time
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Optional

from PyQt5.QtCore import Qt, QEvent, QDataStream, QIODevice, QRectF, QPointF
from PyQt5.QtGui import QMouseEvent, QTransform, QPainter, QDropEvent, QPixmap, QWheelEvent, QCursor
from PyQt5.QtWidgets import QGraphicsView, QMenu, QAction, QGraphicsProxyWidget
from base_classes import BaseGraphicsItem

from .scene import Scene
from connection import Line, Connection
from node import Node
from port import Port
if TYPE_CHECKING:
    from .display_widget import DisplayWidget

class View(QGraphicsView):
    """The QGraphicsView for the NodeEditorWidget
    
    Attributes:
        is_entered: Whether the cursor is within the area of the View.
    """
    def __init__(self, scene: Scene, display_widget: 'DisplayWidget') -> None:
        super().__init__(scene)
        self.scene = scene
        self.display_widget = display_widget
        self.setAcceptDrops(True)
        self.setRenderHints(
            QPainter.Antialiasing # type: ignore
            | QPainter.HighQualityAntialiasing 
            | QPainter.TextAntialiasing 
            | QPainter.SmoothPixmapTransform
        )
        self.setMouseTracking(False)
        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self._zoom_in_factor = 1.25
        self._zoom_clamp = True
        self._zoom = 10
        self._zoom_step = 1
        self._zoom_range = [0, 10]
        self._drag = False
        self._shift_drag = False
        self._line = None
        self._connection = None
        self._nearest_port = None
        self._start_port = None
        self._double_click_item = None
        self._right_clicked_item = None
        self.is_entered = True
        self.click_time = 0.0
        self._init_right_click_menu()

    def _init_right_click_menu(self) -> None:
        """Initializes QMenu for mouse right-clicks within the View."""
        # Create right click menu
        self.right_click_menu = QMenu(parent=self)
        self.right_click_menu.setHidden(True)
        # Create cut action
        self.action_cut = QAction('Cu&t', self, shortcut='Ctrl+X', statusTip="Cut to clipboard", 
            triggered=self.scene.cut, enabled=False) #type: ignore
        self.right_click_menu.addAction(self.action_cut)
        # Create copy action
        self.action_copy = QAction('&Copy', self, shortcut='Ctrl+C', 
            statusTip="Copy to clipboard", triggered=self.scene.copy, enabled=False) #type: ignore
        self.right_click_menu.addAction(self.action_copy)
        # Create paste action
        self.action_paste = QAction('&Paste', self, shortcut='Ctrl+V', 
            statusTip="Paste from clipboard", triggered=self.paste, enabled=False) #type: ignore
        self.right_click_menu.addAction(self.action_paste)
        # Create delete action
        self.action_delete = QAction('&Delete', self, shortcut='Del', 
            statusTip="Delete selected items", triggered=self.scene.delete, 
            enabled=False) #type: ignore
        self.right_click_menu.addAction(self.action_delete)
        self.action_edit = QAction('&Edit Properties', self, shortcut='Ctrl+P', 
            statusTip="Edit properties of selected item.", 
            triggered=lambda: self.set_properties_widget(self._right_clicked_item), enabled=False) #type: ignore
        self.right_click_menu.addAction(self.action_edit)
        self.scene.clipboard_signal.connect( #type: ignore
            lambda: self.action_paste.setEnabled(bool(self.scene._clipboard)))
        self.right_click_menu.aboutToShow.connect(lambda: setattr(self, 'is_entered', False))

    @property
    def scene(self) -> Scene:
        """The Scene associated with the View."""
        return super().scene() #type: ignore

    @scene.setter
    def scene(self, scene: Scene) -> None:
        self.setScene(scene)

    def paste(self, pos: 'QPointF' = ...) -> None:
        """Paste the QGraphicsItems currently in the Scene's clipboard.
        
        Calls the Scene's paste() method, passing in the pos arg. If pos arg is
        not specified, pos will be set to the position of the right_click_menu
        attribute.

        Args:
            pos: The position to paste to.
        """
        if not pos:
            pos = self.mapToScene(self.mapFromGlobal(self.right_click_menu.pos()))
        self.scene.paste(pos=pos)

    def dragMoveEvent(self, event) -> None:
        """Must be overridden to allow dragging of QGraphicsItems."""
        pass

    def mouseDoubleClickEvent(self, event: QMouseEvent) -> None:
        """Overrides super method to handle mouse double click events.
        
        Calls the super method. If a BaseGraphicsItem is double clicked,
        set the _double_click_item attribute to the BaseGraphicsItem. On
        release, the _double_click_item will be set to the DisplayWidget's
        PropertyWidget. If no QGraphicsItem is double clicked, unset the
        DisplayWidget's currenty PropertyWidget.

        Args:
            event: The QMouseEvent that triggered this method.
        """
        super().mouseDoubleClickEvent(event)
        event_pos = self.mapToScene(event.pos())
        item = self.scene.itemAt(event_pos, QTransform())
        if item: 
            if isinstance(item, BaseGraphicsItem):
                self._double_click_item = item
                event.accept()
                return
            elif isinstance(item, QGraphicsProxyWidget):
                if item.parentItem() and isinstance(item.parentItem(), BaseGraphicsItem):
                    self._double_click_item = item.parentItem()
                    event.accept()
                    return
        self.display_widget.unset_properties_widget()
        event.ignore()

    def mousePressEvent(self, event: QMouseEvent) -> None:
        """Overrides super method to handle mouse single click events.
        
        Calls the super method. If the middle mouse button was clicked,
        call the _drag_view_event() method to handle dragging the view. If the
        right mouse button was clicked, create a timer to track how long it's
        been pressed and call the _drag_view_event() method to handle dragging 
        the view.

        Args:
            event: The QMouseEvent that triggered this method.
        """
        super().mousePressEvent(event)
        if event.button() == Qt.MiddleButton:
        # Middle mouse button clicked
            self._drag_view_event(event)
        # Right mouse button clicked
        elif event.button() == Qt.RightButton:
            self.click_time = time()
            self._drag_view_event(event)
        else:
            event.ignore()

    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        """Overrides super method to handle mouse movement events.
        
        Calls the super method. If a Port was clicked with the left mouse 
        button while the mouse was being moved, call _drag_connection_event()
        method to handle dragging a Connection. 

        Args:
            event: The QMouseEvent that triggered this method.
        """
        super().mouseMoveEvent(event)
        button = event.buttons()
        if button == Qt.LeftButton:
        # Left mouse button clicked while moving mouse
            items = self.scene.selectedItems() 
            if len(items) != 1:
            # Multiple items currently selected; get the item at event pos
                item = self.scene.itemAt(self.mapToGlobal(event.pos()), QTransform())
            else:
                item = items[0]
            if item and isinstance(items[0], Port):
                port = items[0]
                self._drag_line_event(event, port)
            else:
                event.ignore()
        else:
            event.ignore()   

    def mouseReleaseEvent(self, event: QMouseEvent) -> None:
        """Overrides super method to handle mouse button releases.
        
        Calls the super method. If the middle mouse button was released,
        unset the mode for dragging the View. If the right mouse button 
        was clicked for more than 0.25 secnods then unset the mode for
        dragging the View. If it was pressed for less than 0.25 seconds,
        then call the _right_click_menu_event method to populate the right 
        click menu instead. If the left mouse button was pressed and a single 
        Port was selected at the time of release, call _connection_event() 
        method to handle creating a Connection.

        Args:
            event: The QMouseEvent that triggered this method.
        """
        super().mouseReleaseEvent(event)       
        if event.button() in [Qt.MiddleButton, Qt.RightButton]:
        # Unset dragging view
            fake_event = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
                Qt.LeftButton, event.buttons() & ~Qt.LeftButton, event.modifiers()) #type: ignore
            super().mouseReleaseEvent(fake_event)
            self.setDragMode(QGraphicsView.RubberBandDrag)
            if event.button() == Qt.RightButton and (time() - self.click_time) < 0.25:
                self._right_click_menu_event(event)
            event.accept() 
        elif event.button() == Qt.LeftButton:
        # Left mouse button released
            if self._nearest_port:
            # Unset _nearest_port attribute
                self._nearest_port.is_highlighted = False
                self._nearest_port = None
            if self._double_click_item and isinstance(self._double_click_item, BaseGraphicsItem):
            # Set DisplayWidget's PropertyWidget and unset_double_click_item
                self.display_widget.set_properties_widget(
                    self._double_click_item.properties_widget)
                self._double_click_item = None
                event.accept()
            else:
                selected_items = self.scene.selectedItems()
                if len(selected_items) == 1 and isinstance(selected_items[0], Port):
                # A single Port was selected at time of mouse release.
                # Call connection_event() method, using selected Port as start
                    start_port = selected_items[0]
                    self._connection_event(event, start_port)
                else:
                    event.ignore()
        else:
            event.ignore()

    def _connection_event(self, event: QMouseEvent, start_port: Port) -> None:
        """Handles events related to creating and changing Connections.
        
        If self._line attribute is set, will call _create_connection() 
        method to create a Connection, passing in the start_port arg and 
        the Port at the _line attribute's end_pos; otherwise, if 
        self._connection attribute is set, will call 
        _change_connection_port() to change the end Port the 
        self._connection attribute.

        Args:
            event: The QMouseEvent that triggered this method.
            start_port: The original Port used to start the event. Used as
                either one Port for creating a connection or as the original
                Port to change in a Connection.
        """
        if self._line and self._line.start_pos and self._line.end_pos:
            end_port = self.scene.itemAt(self._line.end_pos, QTransform())
            if isinstance(end_port, Port):
                self._create_connection(start_port, end_port)
            self._line.delete()
            self._line = None
            event.accept()
        elif self._connection:
            end_port = Connection.find_nearest_port(self.scene, self.mapToScene(event.pos()))
            if end_port:
                self.scene.deselect_all()
                end_port.setSelected(True)
                self._change_connection_port(self._connection, start_port, end_port)
                event.accept()
            else:
                self._connection.update()
                event.ignore()
            self._connection = None

    def _drag_line_event(self, event: QMouseEvent, start_port: Port) -> None:
        """Handles events related to dragging Connections and Lines.
        
        If the shift button was pressed when this event was triggered, 
        will call _drag_line() method for the start_port Connection; 
        otherwise, will call _drag_line() method for the self._line
        attribute.

        Args:
            event: The QMouseEvent that triggered this method.
            start_port: The original Port used to start the event. Will 
                be where the Line or Connections are dragged from.
        """
        pos = self.mapToScene(event.pos())
        modifier = event.modifiers()
        if modifier == Qt.ShiftModifier or self._connection:
            self._connection = start_port.connection
            self.scene.deselect_all()
            start_port.setSelected(True)
            if self._connection:
                self._drag_line(self._connection, start_port, pos)
        else:
            if self._line:
                self._drag_line(self._line, start_port, pos)
            else:
                self._line = Line(start_port.scene_pos, pos)
                self.scene.add_connection(self._line)
        event.accept()

    def _right_click_menu_event(self, event: QMouseEvent) -> None:
        """Activates the right_click_menu attribute.
        
        Opens the right click menu at the event position + an offset. If a 
        QGraphicsItem is right-clicked and is not currently selected, will 
        de-select all QGraphicsItem and select the right-click item.

        Args:
            event: The QMouseEvent that triggered this method.
        """
        event_pos = self.mapToScene(event.pos())
        pos = self.mapToGlobal(event.pos())
        item = self.scene.itemAt(event_pos, QTransform())
        if item:
            if isinstance(item, QGraphicsProxyWidget):
                item = item.parentItem()
            if not item.isSelected():
                self.scene.deselect_all()
                item.setSelected(True)
            self.action_edit.setEnabled(True)
            self._right_clicked_item = item
        else:
            self.action_edit.setEnabled(False)
            self._right_clicked_item = None
        self.right_click_menu.move(pos.x()+5, pos.y()+5)
        self.right_click_menu.setHidden(False)
        event.accept()

    def _drag_view_event(self, event: QMouseEvent) -> None:
        """Handles dragging the View around.

        Args:
            event: The QMouseEvent that triggered this method.
        """
        releaseEvent = QMouseEvent(QEvent.MouseButtonRelease, event.localPos(), 
                event.screenPos(), Qt.LeftButton, Qt.NoButton, event.modifiers())
        super().mouseReleaseEvent(releaseEvent)
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        fake_event = QMouseEvent(event.type(), event.localPos(), event.screenPos(),
            Qt.LeftButton, event.buttons() | Qt.LeftButton, event.modifiers()) #type: ignore
        super().mousePressEvent(fake_event)
        event.accept()

    def _drag_line(self, line: Line, start_port: Port, end_pos: QPointF) -> None:
        """Drags a Line.

        If the Line is a Connection and the start_port arg is a producer, 
        the start_pos of the Connection will be dragged to the end_pos arg; 
        otherwise, the Line's end_pos will be dragged to the end_pos arg.

        Args:
            line: The Line to drag from start_port position to end_pos.
            start_port: The Port from which the drag even originated from.
            end_pos: The QPointF position to drag the Connection to.
        """
        line.is_highlighted = True
        if self._nearest_port:
            self._nearest_port.is_highlighted = False
        self._nearest_port = Connection.find_nearest_port(self.scene, end_pos)
        if self._nearest_port:
        # Snap Connection(s) to nearest port
            self._nearest_port.is_highlighted = True
            end_pos = self._nearest_port.scene_pos
        if isinstance(line, Connection) and start_port.is_producer:
            line.start_pos = end_pos
        else:
            line.end_pos = end_pos 
    
    def _create_connection(self, port1: Port, port2: Port) -> bool:
        """Creates a Connection with start_port and Port and end_pos.
        
        Determines which Port is the producer Port and which Port is the 
        consumer Port. Verifies the Connection would be valid. Calls 
        add_connection() method to add the created Connection to the Scene.

        Args:
            start_port: Port on one end of the Connection to create.
            end_port: Port on the other end of the Connection to create.

        Returns:
            Whether a Port was created.
        """
        if Connection.is_valid_connection(port1, port2):
            if port1.is_producer:
                producer_port = port1
                consumer_port = port2
            else:
                producer_port = port2
                consumer_port = port1
            connection = Connection(consumer_port, producer_port)
            self.scene.add_connection(connection)
            return True
        return False

    def _change_connection_port(
        self, 
        connection: Connection, 
        from_port: Port, 
        to_port: Port,
    ) -> bool:
        """Changes the from_port arg in a Connection to the to_port arg.
        
        Verifies the Connection would be valid with the ports changed Calls the
        update() method of the Connection.

        Args:
            connection: The Connection to change.
            from_port: The Port in the Connection to be changed.
            to_port: The Port to change the from_port in the Connection to.

        Returns:
            Whether the Port in the Connection was changed.
        """
        connection.is_highlighted = False
        other_port = connection.get_other_port(from_port)
        if other_port and Connection.is_valid_connection(
            other_port, to_port, reconnection=connection):
            if to_port.is_consumer:
                connection.consumer_port = to_port
            else:
                connection.producer_port = to_port
            return True
        connection.update()
        return False

    def dropEvent(self, event: QDropEvent) -> None:
        """Overrides super method to allow dropping Nodes into View.
        
        Args:
            event: The QDropEvent that triggered this method.
        """
        if event.mimeData().hasFormat('application/x-item'):
            eventData = event.mimeData().data('application/x-item')
            dataStream = QDataStream(eventData, QIODevice.ReadOnly)
            pixmap = QPixmap()
            dataStream >> pixmap # type: ignore
            node_component = dataStream.readQString()
            node_path = Path(Node.node_dict[node_component]['directory'])
            node_package_id = Node.node_dict[node_component]['package_id']
            position = self.mapToScene(event.pos())
            node = Node(node_component, node_path, node_package_id)
            self.scene.add_node(node)
            node.setPos(position)
            self.scene.deselect_all()
            node.setSelected(True)
            event.setDropAction(Qt.MoveAction)
            event.accept()
        else:
            event.ignore()

    def wheelEvent(self, event: QWheelEvent) -> None:
        """Overrides super method to allow zooming View in/out.
        
        Args:
            event: The QWheelEvent that triggered this method.
        """
        # check to not zoom in when right click move in grid is active
        if event.source() == Qt.MouseEventSynthesizedBySystem:
            return
        # calculate zoom Factor
        zoomOutFactor = 1 / self._zoom_in_factor
        # calculate zoom
        if event.angleDelta().y() > 0:
            zoomFactor = self._zoom_in_factor
            self._zoom += self._zoom_step
        else:
            zoomFactor = zoomOutFactor
            self._zoom -= self._zoom_step
        clamped = False
        if self._zoom < self._zoom_range[0]: 
            self._zoom, clamped = self._zoom_range[0], True
        if self._zoom > self._zoom_range[1]: 
            self._zoom, clamped = self._zoom_range[1], True
        # set scene scale
        if not clamped or self._zoom_clamp is False:
            self.scale(zoomFactor, zoomFactor)
        event.accept()

    def get_viewable_area(self) -> QRectF:
        """Returns the currently viewable area of the View.
        
        Returns:
            The QRectF containing the currently viewable area.
        """
        return self.mapToScene(self.viewport().rect()).boundingRect()

    def get_cursor_pos(self) -> QPointF:
        """Returns the current cursor position.
        
        Returns:
            The QPointF contianing the current cursor position.
        """
        return self.mapToScene(self.mapFromGlobal(QCursor.pos()))

    def leaveEvent(self, event: QEvent) -> None:
        """Overridces super method to set is_entered attribute False."""
        self.is_entered = False
        super().leaveEvent(event)

    def enterEvent(self, event: QEvent) -> None:
        """Overridces super method to set is_entered attribute True."""
        self.is_entered = True
        super().enterEvent(event)

    def set_properties_widget(self, item: 'Optional[BaseGraphicsItem]' = ...) -> bool:
        """Sets the properties_widget inside the view's display_widget
        
        If item arg is not provided, will use currently selected item
        if exactly one item is selected. If either no item is selected
        or multiple items are selected, no properties_widget will be 
        set.

        Args:
            item: The BaseGraphicsItem to set the properties_panel for.
        
        Returns:
            Whether a properties_panel was set.
        """
        if item is ...:
            items = self.scene.selectedItems()
            if len(items) == 1 and isinstance(items[0], BaseGraphicsItem):
                item = items[0]
            else:
                item = None
        if item is None:
            return False
        return self.display_widget.set_properties_widget(item.properties_widget)
