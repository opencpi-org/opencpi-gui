#! /usr/bin/python3

import sys

from PyQt5.QtWidgets import QApplication

from main_window import MainWindow

app = QApplication(sys.argv)
app.setStyle('Fusion')
window = MainWindow()

def run() -> int:
    """Runs the node graph application.
    
    Returns:
        Return code of the application .exec_() method.
    """
    return app.exec_()
    
if __name__ == '__main__':
    sys.exit(app.exec_())
