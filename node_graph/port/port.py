from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Dict, Any, Optional

from PyQt5.QtCore import QRectF
from PyQt5.QtGui import QPen, QColor, QBrush, QPainter
if TYPE_CHECKING:
    from PyQt5.QtCore import QPointF
    from PyQt5.QtGui import QPainter
    from PyQt5.QtWidgets import QWidget, QStyleOptionGraphicsItem

from .model import PortModel
from .properties_widget import PortPropertiesWidget
from base_classes import BaseGraphicsItem
if TYPE_CHECKING:
    from node import Node
    from connection import Connection

class Port(BaseGraphicsItem):
    """Graphical representation of a Port.

    Attributes:
        node: The Node that the Port belongs to.
        connection: Connection, if any, from this Port to another Port.
    """
    def __init__(
        self, 
        name: str, 
        properties: 'Dict[str, Any]', 
        node: 'Node', 
        model: 'Optional[PortModel]' = ...,
    ) -> None:
        super().__init__(parent=node)
        if model is ... or model is None:
            model = PortModel(name, properties)
        self._model: PortModel = model
        self.node = node
        self.connection: 'Optional[Connection]' = None
        self._properties_widget = None
        self._init_sizes()
        self._init_assets()

    @property
    def properties_widget(self) -> PortPropertiesWidget:
        """QWidget containing the connection's properties."""
        if self._properties_widget: return self._properties_widget
        self._properties_widget = PortPropertiesWidget(self)
        return self._properties_widget

    @property
    def name(self) -> str:
        """The name of the Port."""
        return self._model.name

    @property
    def type(self) -> str:
        """The Port type: either "consumer" or "producer"."""
        return self._model.type

    @property
    def is_producer(self) -> bool:
        """Whether the Port is of type "producer"."""
        return self._model.is_producer

    @property
    def is_consumer(self) -> bool:
        """Whether the Port is of type "consumer"."""
        return self._model.is_consumer

    @property
    def scene_pos(self) -> 'QPointF':
        """The position of the Port within its Scene."""
        return self.scenePos()

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the Port to a dictionary representation.

        Calls _model.to_dict() to get a dictionary representation of the
        PortModel.

        Returns:
            A Dictionary representation of the Port.
        """
        out_dict = {
            'instance': self.node.name,
            'model': self._model.to_dict(),
            'pos': (self.pos().x(), self.pos().y())
        }
        return out_dict

    @classmethod
    def from_dict(cls, in_dict: 'Dict[str, Any]', node: 'Node') -> 'Port':
        """Creates a Port from a dictionary and belonging to Node arg.

        Args:
            in_dict: The dictionary representation of a Port.
            node: The Node that the Port will belong to.

        Returns:
            A Port based on the in_dict arg and belonging to Node arg.
        """
        model = PortModel.from_dict(in_dict['model'])
        port = cls(model.name, model.properties, model=model, node=node)
        port.setPos(*in_dict['pos'])
        return port

    def _init_assets(self) -> None:
        """Initiates various graphical assets of the Port."""
        if self.is_producer:
            self._color_background = QColor('#ff9a00')
        else:
            self._color_background = QColor('#00a2ff')
        self._color_outline = QColor("#FF000000")
        self._pen = QPen(self._color_outline)
        self._pen.setWidthF(self.outline_width)
        self._pen_highlight = QPen(QColor("#FFFFFF"))
        self._pen_highlight.setWidthF(self.outline_width*1.5)
        self._pen_select = QPen(QColor("#0FFF50"))
        self._pen_select.setWidthF(self.outline_width*1.5)
        self._brush = QBrush(self._color_background)
        

    def _init_sizes(self) -> None:
        """Initiates graphical dimensions of the Port."""
        self.radius = 8
        self.outline_width = 1.5

    def paint(
        self, 
        painter: 'QPainter', 
        option: 'QStyleOptionGraphicsItem', 
        widget: 'QWidget'
    ) -> None:
        """Required to be implemented by QT for a QGraphicsItem.
        
        Sets color of Port dependant on whether Port is currently highlighted, 
        selected, or neither.

        Args:
            painter: Required for call to super.
            option: Required for call to super.
            widget: Required for call to super.
        """
        painter.setBrush(self._brush)
        if self.isSelected():
            painter.setPen(self._pen_select)
        elif self.is_highlighted:
            painter.setPen(self._pen_highlight)
        else:
            painter.setPen(self._pen)
        painter.drawEllipse(-self.radius, -self.radius, 2 * self.radius, 2 * self.radius)

    def boundingRect(self) -> QRectF:
        """Required to be implemented by QT for a QGraphicsItem.
        
        Sets graphical dimensions of the Port.
        """
        return QRectF(
            - self.radius - self.outline_width,
            - self.radius - self.outline_width,
            2 * (self.radius + self.outline_width),
            2 * (self.radius + self.outline_width),
        )
