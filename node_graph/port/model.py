from copy import deepcopy
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Dict, Any

from base_classes import BaseModel

class PortModel(BaseModel):
    """Models the data of a Port.

    Attributes:
        properties: A dictionary containing the PortModel's properties.
        name: The name of the Port.
    """
    def __init__(self, name: str, properties: 'Dict[str, Any]') -> None:
        super().__init__()
        self.name = name
        self.properties = properties

    @property
    def type(self) -> str:
        """The type of the Port: either "producer" or "consumer"."""
        return 'producer' if self.is_producer else 'consumer'

    @property
    def is_producer(self) -> bool:
        """Whether the Port is of type "producer"."""
        if 'producer' not in self.properties:
            return False
        if self.properties['producer'].lower() in ['false', '0']:
            return False
        return True

    @property
    def is_consumer(self) -> bool:
        """Whether the Port is of type "consumer"."""
        return not self.is_producer

    def to_dict(self) -> 'Dict[str, Any]':
        """Converts the PortModel to a dictionary representation.

        Returns:
            A dictionary containing the PortModel's name attribute and a copy 
            of the PortModel's properties attribute.
        """
        out_dict = {
            'name': self.name,
            'properties': deepcopy(self.properties)
        }
        return out_dict

    @classmethod
    def from_dict(cls, in_dict: 'Dict[str, Any]') -> 'PortModel':
        """Creates a PortModel from a dictionary.

        Args:
            in_dict: The dictionary representation of a PortModel.

        Returns:
            A PortModel based on the in_dict arg.
        """
        name = in_dict['name']
        properties = in_dict['properties']
        model = cls(name, properties=properties)
        return model
        