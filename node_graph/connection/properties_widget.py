from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Tuple

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QLineEdit, QGroupBox, QVBoxLayout

from base_classes import BasePropertiesWidget
if TYPE_CHECKING:
    from .connection import Connection

class ConnectionPropertiesWidget(BasePropertiesWidget):
    """Container widget for widgets related to a Connection's properties.

    Attributes:
        consumer_port_widget: QLabel containing the name of the consumer Port 
            of the Connection.
        producer_port_widget: QLabel containing the name of the producer Port 
            of the Connection.
        consumer_component_widget: QLabel for the name of the component 
            containining the consumer Port of the Connection.
        producer_component_widget: QLabel for the name of the component 
            containining the producer Port of the Connection.
        consumer_instance_widget: QLabel for the name of the Node containining
            the consumer Port of the Connection.
        producer_instance_widget: QLabel for the name of the Node containining
            the producer Port of the Connection.
        consumer_bufferCount_widget: QLineEdit for the bufferCount of for the
            consumer Port of the Connection.
        producer_bufferCount_widget: QLineEdit for the bufferCount of for the
            producer Port of the Connection.
    """
    def __init__(self, connection: 'Connection') -> None:
        self._connection = connection
        self.consumer_component_widget = QLabel()
        self.consumer_instance_widget = QLabel()
        self.consumer_port_widget = QLabel()
        self.consumer_bufferCount_widget = QLineEdit()
        self.producer_component_widget = QLabel()
        self.producer_instance_widget = QLabel()
        self.producer_port_widget = QLabel()
        self.producer_bufferCount_widget = QLineEdit()
        super().__init__()

    def _init_widgets(self) -> None:
        """Initializes the QWidgets for the Connection's properties."""
        widgets = []
        label = QLabel('Connection')
        label.setAlignment(Qt.AlignHCenter)
        widgets.append(label)
        for port in self._connection.ports:
            if not port:
                continue
            vbox = QVBoxLayout()
            vbox.setSpacing(5)
            groupbox = QGroupBox(port.type.capitalize())
            groupbox.setLayout(vbox)
            comp_widget, instance_widget, port_widget, buffer_widget = self.get_port_widgets(port)
            instance_widget.setText(f'Instance: {port.node.name}')
            vbox.addWidget(instance_widget)
            comp_widget.setText(f'Component: {port.node.component}')
            vbox.addWidget(comp_widget)
            port_widget.setText(f'Port: {port.name}')
            vbox.addWidget(port_widget)
            property = self._connection._model.properties[f'{port.type}_bufferCount']
            value = property.get('value', '')
            vbox.addWidget(QLabel('bufferCount'))
            buffer_widget.setText(value)
            if port.is_producer:
                # Update model when consumer buffer field changed
                buffer_widget.textChanged.connect(lambda value: self._connection.save(
                    self._connection._model.properties['producer_bufferCount'], value))
                # Update produce instance name label when name is changed
                producer_name_line = port.node.properties_widget.name_line
                port.node.properties_widget.name_line.editingFinished.connect(
                    lambda: self.producer_instance_widget.setText(
                    f'Instance: {producer_name_line.text()}'))
            else:
                # Update model when consumer buffer field changed
                buffer_widget.textChanged.connect(lambda value: self._connection.save(
                    self._connection._model.properties['consumer_bufferCount'], value))
                # Update consumer instance name label when name is changed
                consumer_name_line = port.node.properties_widget.name_line
                consumer_name_line.editingFinished.connect(
                    lambda: self.consumer_instance_widget.setText(
                    f'Instance: {consumer_name_line.text()}'))
            vbox.addWidget(buffer_widget)
            widgets.append(groupbox)
        groupbox = QGroupBox('BufferSize')
        vbox = QVBoxLayout()
        vbox.setSpacing(5)
        groupbox.setLayout(vbox)
        bufferSize_widget = QLineEdit()
        value = self._connection._model.properties['bufferSize'].get('value', '')
        bufferSize_widget.setText(value)
        bufferSize_widget.textChanged.connect(
            lambda value: self._connection.save(
                self._connection._model.properties['bufferSize'], value))
        vbox.addWidget(bufferSize_widget)
        widgets.append(groupbox)
        self.addWidgets(widgets)

    def get_port_widgets(self, port) -> 'Tuple[QLabel, QLabel, QLabel, QLineEdit]':
        """Returns the appropriate QWidgets for a Port of a given type.
        
        Args:
            port: The Port to return the appropriate QWidgets for.

        Returns:
            A tuple consisting of the component_widget, instance_widget, 
            port_widget, and bufferCount_widget appropriate for the type of
            the port arg.
        """
        if port.is_consumer:
            return (
                self.consumer_component_widget, 
                self.consumer_instance_widget, 
                self.consumer_port_widget, 
                self.consumer_bufferCount_widget
            )
        else:
            return (
                self.producer_component_widget,
                self.producer_instance_widget, 
                self.producer_port_widget, 
                self.producer_bufferCount_widget
            )
