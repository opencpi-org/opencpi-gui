from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from typing import Optional, Union, Dict, List, Literal

import PyQt5.QtCore as QtCore
from PyQt5.QtGui import QPixmap, QDrag, QPainter
from PyQt5.QtWidgets import (QTreeWidget, QAbstractItemView, QTreeWidgetItem, QWidget,
    QStyledItemDelegate, QStyleOptionViewItem, QStyle)

from node import Node

class NodeTreeWidget(QTreeWidget):
    """QListWidget containing list of Nodes user can drag into node editor."""
    def __init__(self, parent: 'Optional[QWidget]' = ...) -> None:
        if parent is ...:
            parent = None
        super().__init__(parent)    
        self.setDragEnabled(True)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setSortingEnabled(True)
        self.sortByColumn(0, QtCore.Qt.AscendingOrder)
        self.tree_dict = {}
        self.items: 'List[QTreeWidgetItem]' = []
        self.init_tree()
        self.setAlternatingRowColors(True)
        self.setItemDelegate(StyleItemDelegate(self))
        self.filter_str = ''

    def init_tree(self):
        """Initializes the tree with elements."""
        for node in Node.node_dict:
            prefix, project, name = node.split('.', 2)
            if '.' in name:
                library, name = name.split('.', 1)
            else:
                library = None
            if prefix not in self.tree_dict:
                self.tree_dict[prefix] = {}
            if project not in self.tree_dict[prefix]:
                self.tree_dict[prefix][project] = {} if library else []
            if library:
                if library not in self.tree_dict[prefix][project]:
                    self.tree_dict[prefix][project][library] = []
                self.tree_dict[prefix][project][library].append(node)
            else:
                self.tree_dict[prefix][project].append(node)
        self.init_items()

    def init_items(self, parent: 'Union[QTreeWidget, QTreeWidgetItem]' = ..., 
        items: 'Union[Dict, List]' = ...) -> None:
        """Initializes items to insert into tree.

        Recursively calls self until reaching bottom level elements.
        
        Args:
            parent: Widget for items to be inserted as children of.
            items: Dict or List of items to initialize.
        """
        if items is ...:
            items = self.tree_dict
        if parent is ...:
            parent = self
        if isinstance(items, list):
        # Bottom level, so create node items to insert into tree
            for item in items:
                widget = self.init_item(parent, item, is_bottom_level=True)
                widget.setData(0, QtCore.Qt.UserRole, 'child')
                self.items.append(widget)
        else:
        # Create a parent level item to insert into tree and recurse
            for name, props in items.items():
                widget = self.init_item(parent, name)
                self.init_items(widget, props)

    def init_item(self, parent: 'Union[QTreeWidget, QTreeWidgetItem]', name: str, 
        is_bottom_level: bool = False, type: 'Literal[0, 1, 2]' = 0) -> QTreeWidgetItem:
        """ Initializes and returns and a QTreeWidgetItem.
        
        Args:
            parent: Widget for item to be inserted as a child of.
            name: The text for the item to display.
            is_bottom_level: Whether item is the bottom of a branch.
            type: The int value of show expand/colapse arrow.
            
        Returns:
            A QTReeWidgetItem
        """
        widget = QTreeWidgetItem(parent, type=type)
        widget.setText(0, name.split('.')[-1])
        widget.setToolTip(0, name)
        if is_bottom_level:
            widget.setText(1, name)
        return widget

    def startDrag(self, *args, **kwargs) -> None:
        """Handles dragging Nodes from NodeListWidget into node editor."""
        node = self.currentItem()
        if not node.text(1):
        # Don't allow dragging of non-bottom level items
            return
        pixmap = QPixmap(node.data(0, QtCore.Qt.UserRole))
        itemData = QtCore.QByteArray()
        dataStream = QtCore.QDataStream(itemData, QtCore.QIODevice.WriteOnly)
        dataStream << pixmap # type: ignore
        node_name = '.'.join([node.text(1)])
        dataStream.writeQString(node_name)
        mimeData = QtCore.QMimeData()
        mimeData.setData('application/x-item', itemData)
        drag = QDrag(self)
        drag.setMimeData(mimeData)
        drag.setHotSpot(QtCore.QPoint(pixmap.width() // 2, pixmap.height() // 2))
        drag.setPixmap(pixmap)
        drag.exec_(QtCore.Qt.MoveAction)

    def filter_items(self, filter_str: str) -> None:
        """Hides items in tree that do not match give filter_str.

        Args:
            filter_str: String used to search within items for.
        """
        self.filter_str = filter_str
        if filter_str == '':
            self.collapseAll()
        else:
            self.expandAll()
            for item in self.items:
                item.setHidden(True)
        items = self.findItems(
            filter_str, QtCore.Qt.MatchContains | QtCore.Qt.MatchRecursive) # type: ignore
        for item in items:
            item.setHidden(False)

class StyleItemDelegate(QStyledItemDelegate):

    def paint(self, painter: QPainter, option: 'QStyleOptionViewItem', 
        index: QtCore.QModelIndex) -> None:
        """Overrides super to paint data.
        
        If parent item has a filter_str attribute set, highlights
        the matching filter_str within index.data(). Used to highlight
        part of NodeTreeWidget item that matches a given search string.

        Args:
            painter: Required by super.
            option: Required by super.
            index: Required by super.
        """
        if (option.state & QStyle.State_Selected): # type: ignore
            return super().paint(painter, option, index)
        if index.data(QtCore.Qt.UserRole) == 'child':
            if self.parent() and self.parent().filter_str:
                match_str = self.parent().filter_str
                data = index.data()
                match_idx = data.find(match_str)
                if match_idx != -1:
                    left_width = painter.fontMetrics().width(data[:match_idx])
                    match_width = painter.fontMetrics().width(match_str)
                    match_rect = QtCore.QRect(option.rect.left() + left_width, 
                                              option.rect.top(), 
                                              match_width, 
                                              option.rect.height())
                    painter.fillRect(match_rect, QtCore.Qt.lightGray)
        painter.drawText(option.rect, option.displayAlignment, index.data()) #type: ignore
        