# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_project_path/project_explorer_path.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Project_Path_Dialog(object):
    def setupUi(self, Project_Path_Dialog):
        Project_Path_Dialog.setObjectName("Project_Path_Dialog")
        Project_Path_Dialog.resize(400, 168)
        self.verticalLayout = QtWidgets.QVBoxLayout(Project_Path_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(Project_Path_Dialog)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.Proj_Path_lineEdit = QtWidgets.QLineEdit(Project_Path_Dialog)
        self.Proj_Path_lineEdit.setPlaceholderText("")
        self.Proj_Path_lineEdit.setObjectName("Proj_Path_lineEdit")
        self.verticalLayout.addWidget(self.Proj_Path_lineEdit)
        self.toolButton = QtWidgets.QToolButton(Project_Path_Dialog)
        self.toolButton.setObjectName("toolButton")
        self.verticalLayout.addWidget(self.toolButton)
        self.buttonBox = QtWidgets.QDialogButtonBox(Project_Path_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Project_Path_Dialog)
        self.buttonBox.accepted.connect(Project_Path_Dialog.accept)
        self.buttonBox.rejected.connect(Project_Path_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Project_Path_Dialog)

    def retranslateUi(self, Project_Path_Dialog):
        _translate = QtCore.QCoreApplication.translate
        Project_Path_Dialog.setWindowTitle(_translate("Project_Path_Dialog", "Project Explorer Path"))
        self.label.setText(_translate("Project_Path_Dialog", "<html><head/><body><p>The parent directory of the project that will be shown in Project Explorer. </p></body></html>"))
        self.toolButton.setText(_translate("Project_Path_Dialog", "..."))

