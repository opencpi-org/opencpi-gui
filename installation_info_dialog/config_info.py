# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './installation_info_dialog/opencpi_config_info.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Installation_Info_Dialog(object):
    def setupUi(self, Installation_Info_Dialog):
        Installation_Info_Dialog.setObjectName("Installation_Info_Dialog")
        Installation_Info_Dialog.resize(490, 294)
        self.gridLayout = QtWidgets.QGridLayout(Installation_Info_Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(Installation_Info_Dialog)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.CDK_Location_label = QtWidgets.QLabel(Installation_Info_Dialog)
        self.CDK_Location_label.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.CDK_Location_label.setText("")
        self.CDK_Location_label.setObjectName("CDK_Location_label")
        self.gridLayout.addWidget(self.CDK_Location_label, 0, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(Installation_Info_Dialog)
        self.label_2.setWordWrap(True)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 2)
        self.scrollArea = QtWidgets.QScrollArea(Installation_Info_Dialog)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 355, 214))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.User_Env_Settings_textBrowser = QtWidgets.QTextBrowser(self.scrollAreaWidgetContents)
        self.User_Env_Settings_textBrowser.setObjectName("User_Env_Settings_textBrowser")
        self.verticalLayout.addWidget(self.User_Env_Settings_textBrowser)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.gridLayout.addWidget(self.scrollArea, 1, 2, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Installation_Info_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 1, 1, 2)

        self.retranslateUi(Installation_Info_Dialog)
        self.buttonBox.accepted.connect(Installation_Info_Dialog.accept)
        self.buttonBox.rejected.connect(Installation_Info_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Installation_Info_Dialog)

    def retranslateUi(self, Installation_Info_Dialog):
        _translate = QtCore.QCoreApplication.translate
        Installation_Info_Dialog.setWindowTitle(_translate("Installation_Info_Dialog", "Installation Information"))
        self.label.setText(_translate("Installation_Info_Dialog", "CDK location"))
        self.label_2.setText(_translate("Installation_Info_Dialog", "Enabled User Environment Settings"))

