# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_hdl_platform_dialog/new_hdl_platform_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_HDL_Platform_Dialog(object):
    def setupUi(self, New_HDL_Platform_Dialog):
        New_HDL_Platform_Dialog.setObjectName("New_HDL_Platform_Dialog")
        New_HDL_Platform_Dialog.resize(400, 227)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_HDL_Platform_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(New_HDL_Platform_Dialog)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.Platform_Name_lineEdit = QtWidgets.QLineEdit(New_HDL_Platform_Dialog)
        self.Platform_Name_lineEdit.setObjectName("Platform_Name_lineEdit")
        self.horizontalLayout.addWidget(self.Platform_Name_lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(New_HDL_Platform_Dialog)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.Project_comboBox = QtWidgets.QComboBox(New_HDL_Platform_Dialog)
        self.Project_comboBox.setObjectName("Project_comboBox")
        self.horizontalLayout_2.addWidget(self.Project_comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(New_HDL_Platform_Dialog)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.Part_Number_lineEdit = QtWidgets.QLineEdit(New_HDL_Platform_Dialog)
        self.Part_Number_lineEdit.setText("")
        self.Part_Number_lineEdit.setObjectName("Part_Number_lineEdit")
        self.horizontalLayout_3.addWidget(self.Part_Number_lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_4 = QtWidgets.QLabel(New_HDL_Platform_Dialog)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_4.addWidget(self.label_4)
        self.Time_Server_Freq_lineEdit = QtWidgets.QLineEdit(New_HDL_Platform_Dialog)
        self.Time_Server_Freq_lineEdit.setText("")
        self.Time_Server_Freq_lineEdit.setObjectName("Time_Server_Freq_lineEdit")
        self.horizontalLayout_4.addWidget(self.Time_Server_Freq_lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_HDL_Platform_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_HDL_Platform_Dialog)
        self.buttonBox.accepted.connect(New_HDL_Platform_Dialog.accept)
        self.buttonBox.rejected.connect(New_HDL_Platform_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_HDL_Platform_Dialog)

    def retranslateUi(self, New_HDL_Platform_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_HDL_Platform_Dialog.setWindowTitle(_translate("New_HDL_Platform_Dialog", "New HDL Platform"))
        self.label.setText(_translate("New_HDL_Platform_Dialog", "HDL Platform Name"))
        self.label_2.setText(_translate("New_HDL_Platform_Dialog", "Associated Project"))
        self.label_3.setText(_translate("New_HDL_Platform_Dialog", "Part Number"))
        self.label_4.setText(_translate("New_HDL_Platform_Dialog", "Time Server Freq"))

