# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_component_dialog/new_component_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_Component_Dialog(object):
    def setupUi(self, New_Component_Dialog):
        New_Component_Dialog.setObjectName("New_Component_Dialog")
        New_Component_Dialog.resize(400, 154)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_Component_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(New_Component_Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.Component_Name_lineEdit = QtWidgets.QLineEdit(New_Component_Dialog)
        self.Component_Name_lineEdit.setObjectName("Component_Name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.Component_Name_lineEdit)
        self.Component_Project_comboBox = QtWidgets.QComboBox(New_Component_Dialog)
        self.Component_Project_comboBox.setObjectName("Component_Project_comboBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Component_Project_comboBox)
        self.label_2 = QtWidgets.QLabel(New_Component_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Component_Library_comboBox = QtWidgets.QComboBox(New_Component_Dialog)
        self.Component_Library_comboBox.setObjectName("Component_Library_comboBox")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.Component_Library_comboBox)
        self.label_3 = QtWidgets.QLabel(New_Component_Dialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_Component_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_Component_Dialog)
        self.buttonBox.accepted.connect(New_Component_Dialog.accept)
        self.buttonBox.rejected.connect(New_Component_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_Component_Dialog)

    def retranslateUi(self, New_Component_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_Component_Dialog.setWindowTitle(_translate("New_Component_Dialog", "New Component"))
        self.label.setText(_translate("New_Component_Dialog", "Component Name"))
        self.label_2.setText(_translate("New_Component_Dialog", "Associated Project"))
        self.label_3.setText(_translate("New_Component_Dialog", "Associated Library"))

