# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_hdl_prim_library/new_hdl_prim_library_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_HDL_Prim_Library_Dialog(object):
    def setupUi(self, New_HDL_Prim_Library_Dialog):
        New_HDL_Prim_Library_Dialog.setObjectName("New_HDL_Prim_Library_Dialog")
        New_HDL_Prim_Library_Dialog.resize(400, 132)
        self.verticalLayout = QtWidgets.QVBoxLayout(New_HDL_Prim_Library_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(New_HDL_Prim_Library_Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.HDL_Prim_Lib_Name_lineEdit = QtWidgets.QLineEdit(New_HDL_Prim_Library_Dialog)
        self.HDL_Prim_Lib_Name_lineEdit.setObjectName("HDL_Prim_Lib_Name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.HDL_Prim_Lib_Name_lineEdit)
        self.label_2 = QtWidgets.QLabel(New_HDL_Prim_Library_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Project_comboBox = QtWidgets.QComboBox(New_HDL_Prim_Library_Dialog)
        self.Project_comboBox.setObjectName("Project_comboBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Project_comboBox)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_HDL_Prim_Library_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(New_HDL_Prim_Library_Dialog)
        self.buttonBox.accepted.connect(New_HDL_Prim_Library_Dialog.accept)
        self.buttonBox.rejected.connect(New_HDL_Prim_Library_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_HDL_Prim_Library_Dialog)

    def retranslateUi(self, New_HDL_Prim_Library_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_HDL_Prim_Library_Dialog.setWindowTitle(_translate("New_HDL_Prim_Library_Dialog", "New HDL Primitive Library"))
        self.label.setText(_translate("New_HDL_Prim_Library_Dialog", "Primitive Lib Name"))
        self.label_2.setText(_translate("New_HDL_Prim_Library_Dialog", "Associated Project"))

