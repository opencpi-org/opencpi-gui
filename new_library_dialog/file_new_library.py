# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './new_library_dialog/new_library_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_New_Library_Dialog(object):
    def setupUi(self, New_Library_Dialog):
        New_Library_Dialog.setObjectName("New_Library_Dialog")
        New_Library_Dialog.resize(393, 111)
        self.formLayout = QtWidgets.QFormLayout(New_Library_Dialog)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(New_Library_Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.Library_Name_lineEdit = QtWidgets.QLineEdit(New_Library_Dialog)
        self.Library_Name_lineEdit.setObjectName("Library_Name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.Library_Name_lineEdit)
        self.label_2 = QtWidgets.QLabel(New_Library_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Library_Project_comboBox = QtWidgets.QComboBox(New_Library_Dialog)
        self.Library_Project_comboBox.setObjectName("Library_Project_comboBox")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Library_Project_comboBox)
        self.buttonBox = QtWidgets.QDialogButtonBox(New_Library_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.SpanningRole, self.buttonBox)

        self.retranslateUi(New_Library_Dialog)
        self.buttonBox.accepted.connect(New_Library_Dialog.accept)
        self.buttonBox.rejected.connect(New_Library_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(New_Library_Dialog)

    def retranslateUi(self, New_Library_Dialog):
        _translate = QtCore.QCoreApplication.translate
        New_Library_Dialog.setWindowTitle(_translate("New_Library_Dialog", "New Library"))
        self.label.setText(_translate("New_Library_Dialog", "Library Name"))
        self.label_2.setText(_translate("New_Library_Dialog", "Associated Project"))

