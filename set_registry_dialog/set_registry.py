# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './set_registry_dialog/set_reg_gui.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Set_Reg_Dialog(object):
    def setupUi(self, Set_Reg_Dialog):
        Set_Reg_Dialog.setObjectName("Set_Reg_Dialog")
        Set_Reg_Dialog.resize(400, 181)
        self.verticalLayout = QtWidgets.QVBoxLayout(Set_Reg_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(Set_Reg_Dialog)
        self.label_3.setWordWrap(True)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.label = QtWidgets.QLabel(Set_Reg_Dialog)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.Reg_Path_lineEdit = QtWidgets.QLineEdit(Set_Reg_Dialog)
        self.Reg_Path_lineEdit.setObjectName("Reg_Path_lineEdit")
        self.verticalLayout.addWidget(self.Reg_Path_lineEdit)
        self.toolButton = QtWidgets.QToolButton(Set_Reg_Dialog)
        self.toolButton.setObjectName("toolButton")
        self.verticalLayout.addWidget(self.toolButton)
        self.buttonBox = QtWidgets.QDialogButtonBox(Set_Reg_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Set_Reg_Dialog)
        self.buttonBox.accepted.connect(Set_Reg_Dialog.accept)
        self.buttonBox.rejected.connect(Set_Reg_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Set_Reg_Dialog)

    def retranslateUi(self, Set_Reg_Dialog):
        _translate = QtCore.QCoreApplication.translate
        Set_Reg_Dialog.setWindowTitle(_translate("Set_Reg_Dialog", "Set Project Registry"))
        self.label_3.setText(_translate("Set_Reg_Dialog", "Select the registry for the selected project."))
        self.label.setText(_translate("Set_Reg_Dialog", "Registry location"))
        self.toolButton.setText(_translate("Set_Reg_Dialog", "..."))

