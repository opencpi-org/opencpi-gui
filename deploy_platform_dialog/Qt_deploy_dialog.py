# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './deploy_platform_dialog/deploy_platform_layout.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Deploy_Platform_Dialog(object):
    def setupUi(self, Deploy_Platform_Dialog):
        Deploy_Platform_Dialog.setObjectName("Deploy_Platform_Dialog")
        Deploy_Platform_Dialog.resize(463, 158)
        self.buttonBox = QtWidgets.QDialogButtonBox(Deploy_Platform_Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(30, 100, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.label = QtWidgets.QLabel(Deploy_Platform_Dialog)
        self.label.setGeometry(QtCore.QRect(20, 10, 111, 19))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Deploy_Platform_Dialog)
        self.label_2.setGeometry(QtCore.QRect(20, 50, 111, 19))
        self.label_2.setObjectName("label_2")
        self.RCC_Platform_comboBox = QtWidgets.QComboBox(Deploy_Platform_Dialog)
        self.RCC_Platform_comboBox.setGeometry(QtCore.QRect(140, 10, 241, 27))
        self.RCC_Platform_comboBox.setObjectName("RCC_Platform_comboBox")
        self.HDL_Platform_comboBox = QtWidgets.QComboBox(Deploy_Platform_Dialog)
        self.HDL_Platform_comboBox.setGeometry(QtCore.QRect(140, 50, 241, 27))
        self.HDL_Platform_comboBox.setObjectName("HDL_Platform_comboBox")

        self.retranslateUi(Deploy_Platform_Dialog)
        self.buttonBox.accepted.connect(Deploy_Platform_Dialog.accept)
        self.buttonBox.rejected.connect(Deploy_Platform_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Deploy_Platform_Dialog)

    def retranslateUi(self, Deploy_Platform_Dialog):
        _translate = QtCore.QCoreApplication.translate
        Deploy_Platform_Dialog.setWindowTitle(_translate("Deploy_Platform_Dialog", "Deploy Platform"))
        self.label.setText(_translate("Deploy_Platform_Dialog", "RCC Platform"))
        self.label_2.setText(_translate("Deploy_Platform_Dialog", "HDL Platform"))

