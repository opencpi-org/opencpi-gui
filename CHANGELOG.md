# [v2.4.6](https://gitlab.com/opencpi/ie-gui/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/ie-gui/-/compare/v2.4.4...v2.4.5) (2023-03-16)

Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

### Enhancements
- **tests**: add gui automation steps. (!236)(82663f3f)

# [v2.4.4](https://gitlab.com/opencpi/ie-gui/-/compare/v2.4.3...v2.4.4) (2023-01-22)

Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

### Enhancements
- **doc**: Update user guide for GUI to reflect changes in the GUI. (!226)(cdb7568a)
- **doc**: Update user guide to include the node graph for alpha release. (!226)(cdb7568a)
- **tests**: add gui automation steps. (!232)(c2f317f2)

### Bug Fixes
- **tools**: fix gui from crashing when stopping a job that was in the list but has been cleared out. (!227)(92b1f41a)

# [v2.4.3](https://gitlab.com/opencpi/ie-gui/-/compare/v2.4.2...v2.4.3) (2022-10-11)

Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

### Enhancements
- **tools**: add hdl refresh button. (!195)(00fe11c9)
- **tools**: add a button to the gui that refreshes the rcc platform list. (!196)(912e45d3)
- **tools**: remove unnecessary usage of source opencpi-setup.sh in the gui. (!197)(4db7739a)
- **tools**: make right click menu deal with .comp folders, and more easy to add future changes. (!198)(2d611fae)
- **tools**: fix build select and clean select to deal with all verbs of build and clean. (!201)(3d62309d)
- **tools**: refactor build and clean in gui to use -d flag. (!201)(3d62309d)
- **tools**: have flags come first before verb, noun, and name. (!205)(4936f31b)
- **tools**: Populate correct options when right clicking xml files. (!209)(713b606a)
- **doc**: OpenCPI GUI Guide: add default editor instructions to installation section. (!219)(3e8fa254)

### Bug Fixes
- **tools**: move manual changes to themes out of Qt_OpenCPI.py. (!194)(eed494a4)
- **tools**: added try except to import so gui doesn't crash when not sourced. (!199)(10e71318)
- **tools**: fix is_ocpi_sourced to exist when you are sourced. (!200)(ff9f976c)
- **tools**: gui will not crash when hitting cancel twice on popups. (!202)(dd394b10)
- **tools**: gui will now only look and create config files in the ie-gui folder. (!202)(dd394b10)
- **tools**: print std err to console instead and send stderr to pipe. (!204)(c7d64ff4)

# [v2.4.2](https://gitlab.com/opencpi/ie-gui/-/compare/v2.4.1...v2.4.2) (2022-05-26)

Changes/additions since [OpenCPI Release v2.4.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.1)

### Enhancements
- **tools**: add hdl refresh button. (!195)(00fe11c9)
- **tools**: add a button to the gui that refreshes the rcc platform list. (!196)(912e45d3)
- **tools**: remove unnecessary usage of source opencpi-setup.sh in the gui. (!197)(4db7739a)
- **tools**: make right click menu deal with .comp folders, and more easy to add future changes. (!198)(2d611fae)
- **tools**: fix build select and clean select to deal with all verbs of build and clean. (!201)(3d62309d)
- **tools**: refactor build and clean in gui to use -d flag. (!201)(3d62309d)
- **tools**: have flags come first before verb, noun, and name. (!205)(4936f31b)

### Bug Fixes
- **tools**: move manual changes to themes out of Qt_OpenCPI.py. (!194)(eed494a4)
- **tools**: added try except to import so gui doesn't crash when not sourced. (!199)(10e71318)
- **tools**: fix is_ocpi_sourced to exist when you are sourced. (!200)(ff9f976c)
- **tools**: gui will not crash when hitting cancel twice on popups. (!202)(dd394b10)
- **tools**: gui will now only look and create config files in the ie-gui folder. (!202)(dd394b10)
- **tools**: print std err to console instead and send stderr to pipe. (!204)(c7d64ff4)

# [v2.4.1](https://gitlab.com/opencpi/ie-gui/-/compare/v2.4.0...v2.4.1) (2022-03-16)

Changes/additions since [OpenCPI Release v2.4.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.0)

### Enhancements
- **tools**: make adding themes easier by simply putting .qss files into the stylesheets directory. (!188)(c3ebb2d5)
- **tools**: add unicode characters to job manager statuses for accessibility. (!188)(c3ebb2d5)
- **tools**: tweak job manager status colors for accessibility. (!188)(c3ebb2d5)…ls**: fix not being able to select libraries in certain dialogs when only one project exists. (!192)(d1527a05)
- **tools**: fix selecting "delete job" and "jump to location" in job manager crashing GUI. (!192)(d1527a05)
- **tools**: fix dialogs closing after a warning that user left required fields empty. (!193)(44af0be7)

### Miscellaneous
- **tools**: cleanup code related to constructing and executing 'ocpiadmin' commands. (!189)(cd48cda7)
- **tools**: clean up ocpidev create. (!191)(fc789fba)

# [v2.4.0](https://gitlab.com/opencpi/ie-gui/-/compare/v2.3.4...v2.4.0) (2022-01-25)

Changes/additions since [OpenCPI Release v2.3.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.4)

### Enhancements
- **tools**: add workers as needed flag to "ocpidev build" commands. (!175)(421f3c06)
- **tools**: add ability to run single test case. (!176)(799698e2)
- **tools**: add minimal install to the GUI. (!177)(04668271)
- **tools**: can now run install scritp which will create a ocpigui command that will run the GUI for you. (!181)(db086944)
- **tools**: fix install script to use correct path and -f flag for symlinks. (!183)(a70cdbed)
- **tools**: added --project-path and --ocpi-path flags for the ocpigui command. (!184)(52db736e)
- **tools**: made popup windows load information instantly. (!185)(e4d04499)
- **tools**: make right click menu not crash with non top level projects directories. (!187)(0f84ab6d)

### Bug Fixes
- **tools**: make the drop boxes use the user's ocpi path instead of hard coded ~/opencpi. (!178)(155c118d)
- **tools**: fix gui job manager sometimes reporting incorrect status of a job. (!186)(7371a335)

### Miscellaneous
- **tools**: remove the assets view checkbox from the GUI. (!178)(155c118d)

# [v2.3.4](https://gitlab.com/opencpi/ie-gui/-/compare/v2.3.3...v2.3.4) (2021-12-17)
￼	
Changes/additions since [OpenCPI Release v2.3.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.3)
￼	
### Bug Fixes
- **tools**: fix hdl platform list. (!182)(af0375f6)

# [v2.3.3](https://gitlab.com/opencpi/ie-gui/-/compare/v2.3.2...v2.3.3) (2021-11-30)

Changes/additions since [OpenCPI Release v2.3.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.2)

### Bug Fixes
- **doc,doc,tools,tools**: rename the gui user guide. (!180)(5da6f46d)

### Miscellaneous
- **tools**: remove `doc/odt` folder to make life easier for the gen doc scripts. (!179)(1a034dbe)

# [v2.3.2](https://gitlab.com/opencpi/ie-gui/-/compare/v2.3.1...v2.3.2) (2021-11-08)

No Changes/additions since [OpenCPI Release v2.3.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.1)

# [v2.3.1](https://gitlab.com/opencpi/ie-gui/-/compare/v2.3.0...v2.3.1) (2021-10-13)

Changes/additions since [OpenCPI Release v2.3.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0)

### Miscellaneous
- **doc**: update GUI Install doc to tell users to download correct package. (!174)(2c2878c5)

# [v2.3.0](https://gitlab.com/opencpi/ie-gui/-/compare/v2.3.0-rc.1...v2.3.0) (2021-09-07)

Changes/additions since [OpenCPI Release v2.3.0-rc.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.3.0-rc.1)

### New Features
- **tools**: clarify RCC/HDL platform tooltip. (!172)(ab3671ca)

### Miscellaneous
- **tools**: update comments, docstrings, etc. (!173)(230ebc5c)

# [v2.3.0-rc.1](https://gitlab.com/opencpi/ie-gui/-/compare/v2.2.1...v2.3.0-rc.1) (2021-08-26)

Changes/additions since [OpenCPI Release v2.2.1](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.1)

### New Features
- **app**: set keyboard accelerators to GUI menu. (!163)(0d21d896)
- **doc**: change `New Project` dialog to indicate multiple dependencies can be specified. (!154)(f71f7ca6)
- **tests,tools**: multi-select available for RCC/HDL target platforms. (!140)(6bd10535)
- **tools**: dialog to Project Explorer path set to open every time GUI launched. (!149)(a88909d8)
- **tools**: display current Project Explorer path. (!150)(8fe94aa6)
- **tools**: provide for multiple dependencies in `New Project` dialog. (!155)(eac1b8c1)
- **tools**: display path to current, in-use opencpi root directory. (!157)(1de4132a)
- **tools**: update RCC and HDL panels if path to opencpi root dir changes. (!158)(499df0d6)
- **tools**: set HDL list to show uninstalled platforms and update tooltip. (!166)(90e08667)
- **tools**: move initial startup project location dialog prior to main GUI window appearing. (!167)(0a3b4807)
- **tools**: make default project path available for first project path dialog after starting GUI. (!168)(d02c7eeb)
- **tools**: add xml-only capability to new application creation. (!169)(44cce80d)

### Enhancements
- **devops**: update ie-gui project bug report template to include Acceptance Criteria. (!146)(e1ddd1d8)
- **doc**: update OpenCPI Source Setup dialog to remove reference to config file. (!148)(f26cd447)
- **doc**: update `Install Platform` dialog with info about package ID. (!153)(c55611ed)
- **tools**: make right click menu context sensitive for assets. (!159)(17e15824)

### Bug Fixes
- **app**: fix incorrect job color for error. (!161)(76ad95f4)
- **runtime**: fix `Run Application` dialog to accept multiple arguments if specified. (!162)(a9cbca15)
- **tools**: fix "argument has unexpected type 'bytes'" error. (!139)(1ce7766f)
- **tools**: fix "cd: too many arguments" error when creating projects. (!141)(aaeded65)
- **tools**: fix GRC popup notification that occurs even if GNU Radio is installed. (!142)(4781f810)
- **tools**: fix error that occurs due to too many items in returned object to stop worker method. (!143)(5eaf0b6c)
- **tools**: fix main menu Show item with functionality and remove Show menu from Project Explorer context menu. (!144)(b58da907)
- **tools**: fix `Show Registry` to give currently set environment rather than default location. (!145)(d12bfe65)
- **tools**: fix removal of items caused by changes in GUI widgets. (!156)(83c9a750)
- **tools**: fix build and clean for plural nouns. (!159)(17e15824)
- **tools**: fix delete for single applications. (!159)(17e15824)
- **tools**: fix scenario where user tries to build a platform without selecting a platform to build for. (!159)(17e15824)
- **tools**: fix context sensitive menu to work properly on initial startup of GUI. (!160)(74fd03bc)
- **tools**: fix target name for unit tests. (!165)(5075cd09)
- **tools**: fix context sensitive menu to work with fresh clones and when the project explorer path is changed. (!170)(a6273d90)

### Miscellaneous
- **doc**: update user guide with multi-selection ability and host OS is default target. (!147)(fc0476fe)
- **doc**: improve directory path instructions for `Update Project Explorer Path` dialog. (!151)(6ec54343)
- **doc**: update platform target tooltip with more information. (!152)(70b07bd4)
- **doc**: update user doc to state only `Project Explorer` items can be renamed. (!164)(7b58a9aa)

# [v2.2.1](https://gitlab.com/opencpi/ie-gui/-/compare/v2.2.0...v2.2.1) (2021-07-22)

Changes/additions since [OpenCPI Release v2.2.0](https://gitlab.com/opencpi/opencpi/-/releases/v2.2.0)

### New Features
- **doc**: update user doc to state changes to `/opencpi` location requires relaunch of GUI. (!133)(b4a72ecb)
- **tools**: unit tests have `--verbose` flag available. (!123)(57fcd9db)
- **tools**: right-click menu in Job Manager allows selected job to be re-ran. (!134)(b5a404ba)
- **tools**: move Show Registry/Worker menu to main menu bar. (!137)(cbc8d052)

### Enhancements
- **tools**: add ocpi assets view checkbox. (!118)(07234761)
- **tools**: add RCC/HDL platforms to config file to reduce dynamic creation. (!128)(0a75db7c)

### Bug Fixes
- **runtime**: dialog comboboxes correctly populate. (!135)(320557ca)
- **tools**: remove extra ".xml" extension when new application created. (!126)(ab83e4d7)
- **tools**: fix errors in `develop` branch due to merging older branches. (!127)(f4c2444f)
- **tools**: fix notification when unit test has no arguments. (!138)(f8725467)

### Miscellaneous
- **doc**: add README.md and OpenCPI GUI User Documentation to repo. (!122)(2f55c5d3)
- **doc**: update source comments and docstrings. (!125)(f47c6af2)
- **doc**: update user doc to indicate multi-select of RCC/HDL targets is not available. (!136)(81f812a2)
- **tools**: change command output from utf-8 bytes to normal strings. (!124)(fea3c418)

# [v2.2.0](https://gitlab.com/opencpi/ie-gui/-/compare/f15d547d...v2.2.0) (2021-07-08)

Initial release of IE GUI for OpenCPI
