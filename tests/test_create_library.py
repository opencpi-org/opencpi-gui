from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestLibraryDialog:
    def test_lib_name(self, qtbot):
        window = OpenCPI_GUI.NewLibraryDialog()
        window.show()
        qtbot.addWidget(window)

        window.Library_Name_lineEdit.clear()
        qtbot.keyClicks(window.Library_Name_lineEdit, "dummy_lib")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Library_Name_lineEdit.text() == "dummy_lib"

    def test_lib_project(self, qtbot):
        window = OpenCPI_GUI.NewLibraryDialog()
        window.show()
        qtbot.addWidget(window)

        window.Library_Name_lineEdit.clear()
        qtbot.keyClicks(window.Library_Name_lineEdit, "dummy_lib")
        qtbot.keyClicks(window.Library_Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Library_Name_lineEdit.text() == "dummy_lib"
        assert window.Library_Project_comboBox.currentText() == "dummy_proj"
