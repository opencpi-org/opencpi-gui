from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestRegistryDialog:
    def test_project_name(self, qtbot):
        window = OpenCPI_GUI.NewRegistryDialog()
        window.show()
        qtbot.addWidget(window)

        window.Registry_Name_lineEdit.clear()
        qtbot.keyClicks(window.Registry_Name_lineEdit, "dummy_reg")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Registry_Name_lineEdit.text() == "dummy_reg"

    def test_package_prefix(self, qtbot):
        window = OpenCPI_GUI.NewRegistryDialog()
        window.show()
        qtbot.addWidget(window)

        window.Registry_Name_lineEdit.clear()
        window.Registry_Dir_Path_lineEdit.clear()
        qtbot.keyClicks(window.Registry_Name_lineEdit, "dummy_reg")
        qtbot.keyClicks(window.Registry_Dir_Path_lineEdit, "~/Documents")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Registry_Name_lineEdit.text() == "dummy_reg"
        assert window.Registry_Dir_Path_lineEdit.text() == "~/Documents"
