from PyQt5.QtCore import Qt

import OpenCPI_GUI

class TestPrimLibrary:
    def test_prim_library_name(self, qtbot):
        window = OpenCPI_GUI.NewHdlPrimLibDialog()
        window.show()
        qtbot.addWidget(window)

        window.HDL_Prim_Lib_Name_lineEdit.clear()
        qtbot.keyClicks(window.HDL_Prim_Lib_Name_lineEdit, "dummy_prim_lib")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)
        
        assert window.HDL_Prim_Lib_Name_lineEdit.text() == "dummy_prim_lib"
    

    def test_prim_library_project(self, qtbot):
        window = OpenCPI_GUI.NewHdlPrimLibDialog()
        window.show()
        qtbot.addWidget(window)

        window.HDL_Prim_Lib_Name_lineEdit.clear()
        qtbot.keyClicks(window.HDL_Prim_Lib_Name_lineEdit, "dummy_prim_lib")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.HDL_Prim_Lib_Name_lineEdit.text() == "dummy_prim_lib"
        assert window.Project_comboBox.currentText() == "dummy_proj"