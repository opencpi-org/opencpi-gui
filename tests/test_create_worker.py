from PyQt5.QtCore import Qt

import OpenCPI_GUI 

class TestWorker:
    def test_worker_name(self, qtbot):
        window = OpenCPI_GUI.NewWorkerDialog()
        window.show()
        qtbot.addWidget(window)

        window.Worker_Name_lineEdit.clear()
        qtbot.keyClicks(window.Worker_Name_lineEdit, "dummy_worker")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Worker_Name_lineEdit.text() == "dummy_worker"


    def test_worker_project(self, qtbot):
        window = OpenCPI_GUI.NewWorkerDialog()
        window.show()
        qtbot.addWidget(window)

        window.Worker_Name_lineEdit.clear()
        qtbot.keyClicks(window.Worker_Name_lineEdit, "dummy_worker")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Worker_Name_lineEdit.text() == "dummy_worker"
        assert window.Project_comboBox.currentText() == "dummy_proj"

    
    def test_worker_library(self, qtbot):
        window = OpenCPI_GUI.NewWorkerDialog()
        window.show()
        qtbot.addWidget(window)

        window.Worker_Name_lineEdit.clear()
        qtbot.keyClicks(window.Worker_Name_lineEdit, "dummy_worker")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Library_comboBox, "dummy_lib")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Worker_Name_lineEdit.text() == "dummy_worker"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Library_comboBox.currentText() == "dummy_lib"


    def test_worker_component(self, qtbot):
        window = OpenCPI_GUI.NewWorkerDialog()
        window.show()
        qtbot.addWidget(window)

        window.Worker_Name_lineEdit.clear()
        qtbot.keyClicks(window.Worker_Name_lineEdit, "dummy_worker")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Library_comboBox, "dummy_lib")
        qtbot.keyClicks(window.Component_Spec_comboBox, "dummy_comp")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Worker_Name_lineEdit.text() == "dummy_worker"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Library_comboBox.currentText() == "dummy_lib"
        assert window.Component_Spec_comboBox.currentText() == "dummy_comp"


    def test_worker_language(self, qtbot):
        window = OpenCPI_GUI.NewWorkerDialog()
        window.show()
        qtbot.addWidget(window)

        window.Worker_Name_lineEdit.clear()
        qtbot.keyClicks(window.Worker_Name_lineEdit, "dummy_worker")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Library_comboBox, "dummy_lib")
        qtbot.keyClicks(window.Component_Spec_comboBox, "dummy_comp")
        window.Cpp_radioButton.setChecked(True)
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Worker_Name_lineEdit.text() == "dummy_worker"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Library_comboBox.currentText() == "dummy_lib"
        assert window.Component_Spec_comboBox.currentText() == "dummy_comp"
        assert window.Cpp_radioButton.isChecked()


    def test_worker_model(self, qtbot):
        window = OpenCPI_GUI.NewWorkerDialog()
        window.show()
        qtbot.addWidget(window)

        window.Worker_Name_lineEdit.clear()
        qtbot.keyClicks(window.Worker_Name_lineEdit, "dummy_worker")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.keyClicks(window.Library_comboBox, "dummy_lib")
        qtbot.keyClicks(window.Component_Spec_comboBox, "dummy_comp")
        window.Cpp_radioButton.setChecked(True)
        window.RCC_radioButton.setChecked(True)
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Worker_Name_lineEdit.text() == "dummy_worker"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Library_comboBox.currentText() == "dummy_lib"
        assert window.Component_Spec_comboBox.currentText() == "dummy_comp"
        assert window.Cpp_radioButton.isChecked()
        assert window.RCC_radioButton.isChecked()