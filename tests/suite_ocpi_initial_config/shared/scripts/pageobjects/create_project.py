# -*- coding: utf-8 -*-
import squish
import test
from pageobjects import ocpi

FRAME = {"name": "New_Project_Dialog", "type": "NewProjectDialog"}
PROJ_NAME_LINE =  {"name": "Project_Name_lineEdit", "type": "QLineEdit", "window": FRAME}
PROJ_EXPLORER_BUTTON = {"name": "toolButton", "type": "QToolButton",  "window": FRAME}
EXPLORER_FRAME = {"name": "QFileDialog", "type": "QFileDialog"}
PROJ_DIR = {"name": "treeView", "type": "QTreeView"}
FILE_CHOOSE_BUTTON = {"text": "Choose", "type": "QPushButton","window": EXPLORER_FRAME}
PACKAGE_PREFIX_LINE = {"name": "Package_Prefix_lineEdit", "type": "QLineEdit", "window": FRAME}
PACKAGE_NAME_LINE = {"name": "Package_Name_lineEdit", "type": "QLineEdit", "window": FRAME}
PROJ_DEPENDENCIES = {"name": "Proj_Dependency_lineEdit", "type": "QLineEdit", "window": FRAME} 
REGISTER_BOX = {"name": "Register_Proj_checkBox", "type": "QCheckBox", "window": FRAME}
   
def enter(project_name, directory, pkg_prefix, pkg_name, dependencies, register=True):
    """Fills in the create project fields.
    
    Args:
        project_name: Name of new project
        directory: Directory where project will exsist 
        pkg_prefix: Package prefix
        pkg_name: Package name
        dependencies: Project dependencies
        register: Register project (default true)
    """
    test.log('[Project] Fill Fields With Data')
    squish.type(squish.waitForObject(PROJ_NAME_LINE), project_name)
    squish.clickButton(squish.waitForObject(PROJ_EXPLORER_BUTTON))
    squish.mouseClick(squish.waitForObjectItem(PROJ_DIR, directory))
    squish.clickButton(squish.waitForObject(FILE_CHOOSE_BUTTON))
    squish.type(squish.waitForObject(PACKAGE_PREFIX_LINE), pkg_prefix)
    squish.type(squish.waitForObject(PACKAGE_NAME_LINE), pkg_name)
    squish.type(squish.waitForObject(PROJ_DEPENDENCIES), dependencies)
    if register:
        ocpi.click_button(REGISTER_BOX, 'Register_Proj_checkBox')
