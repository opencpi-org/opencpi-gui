# -*- coding: utf-8 -*-
import squish
import test
 
MAIN_WINDOW = {"name": "OpenCPI", "type": "MainWindow", "windowTitle": "OpenCPI GUI"}
OCPI_HEADER_BUTTON = {"name": "menubar", "type": "QMenuBar", "window": MAIN_WINDOW}
OCPI_CONFIG_OPTION = {"name": "menuOCPI", "type": "QMenu"}
OCPI_CREATE_OPTION = {"name": "menuCreate", "type": "QMenu"}
OCPI_SHOW_OPTION = {"name": "menuShow", "type": "QMenu", "window": MAIN_WINDOW}
OCPI_SCROLL_AREA = {"name": "scrollArea", "type": "QScrollArea", "window": MAIN_WINDOW}
CONSOLE_LOG = {"container": OCPI_SCROLL_AREA, "name": "Console_Log_plainTextEdit",
     "type": "QPlainTextEdit"}
COMP_FRAME = {"name": "New_Component_Dialog", "type": "NewComponentDialog"}
LIB_FRAME = {"name": "New_Library_Dialog", "type": "NewLibraryDialog", "visible": 1}
PROJ_FRAME = {"name": "New_Project_Dialog", "type": "NewProjectDialog"}
COMP_CANCEL_BUTTON = {"text": "Cancel", "type": "QPushButton", "window": COMP_FRAME}
COMP_OK_BUTTON = {"text": "OK", "type": "QPushButton", "window": COMP_FRAME}
LIB_OK_BUTTON = {"text": "OK", "type": "QPushButton", "window": LIB_FRAME}    
LIB_CANCEL_BUTTON = {"text": "Cancel", "type": "QPushButton", "window": LIB_FRAME}
PROJ_OK_BUTTON = {"text": "OK", "type": "QPushButton", "window": PROJ_FRAME}
PROJ_CANCEL_BUTTON = {"text": "Cancel", "type": "QPushButton", "window": PROJ_FRAME}

def open_project_path_dialog() -> None:
    """Open the header option to set the project path."""
    test.log('[OCPI] Set Project Path')
    squish.activateItem(squish.waitForObjectItem(OCPI_HEADER_BUTTON, 'OpenCPI Configuration'))
    squish.activateItem(squish.waitForObjectItem(OCPI_CONFIG_OPTION, 'Set Project Explorer path'))
    
def open_source_path_dialog() -> None:
    """Open the header option to set the source path."""
    test.log('[OCPI] Set Source Path')
    squish.activateItem(squish.waitForObjectItem(OCPI_HEADER_BUTTON, 'OpenCPI Configuration'))
    squish.activateItem(squish.waitForObjectItem(OCPI_CONFIG_OPTION, 
        'Set OpenCPI install directory'))
    
def open_project_dialog() -> None:
    """Open the create new project window."""
    test.log('[OCPI] Create New Project')
    squish.activateItem(squish.waitForObjectItem(OCPI_HEADER_BUTTON, 'Create'))
    squish.activateItem(squish.waitForObjectItem(OCPI_CREATE_OPTION, 'Project...'))
    
def open_component_library() -> None:
    """Open the create new component library window."""
    test.log("[OCPI] Create Component Library")
    squish.activateItem(squish.waitForObjectItem(OCPI_HEADER_BUTTON, "Create"))
    squish.activateItem(squish.waitForObjectItem(OCPI_CREATE_OPTION, "Component Library..."))

def open_component_spec() -> None:
    """Open the create new component spec window."""
    test.log('[OCPI] Create Component Spec')
    squish.activateItem(squish.waitForObjectItem(OCPI_HEADER_BUTTON, 'Create'))
    squish.activateItem(squish.waitForObjectItem(OCPI_CREATE_OPTION, 'Component Spec...'))

def cancel(dialog: dict) -> None:
    """Clicks the "Cancel" button in dialogs.
    
    Args:
        dialog: button in dialog window to click
    """
    button = {"text": "Cancel", "type": "QPushButton", "window": dialog}
    click_button(button, 'Cancel')
    
def confirm(dialog: dict) -> None:
    """Clicks the "OK" button in dialogs.
    
    Args:
        button: button in dialog window to click
    """
    button = {"text": "OK", "type": "QPushButton", "window": dialog}
    click_button(button, 'OK')
    
def click_button(button: dict, button_name: str) -> None:
    """Clicks the given button in dialogs.
    
    Args:
        button: button to click in the dialog
        button_name: name of button to click in dialog
    """
    test.log(f'Clicking "{button_name}" Button')
    squish.clickButton(squish.waitForObject(button))
    test.log(f'Clicked "{button_name}" Button')
    
    
    