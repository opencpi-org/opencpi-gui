# -*- coding: utf-8 -*-
import squish
import test
from pageobjects import ocpi 
from pathlib import Path 
from os import environ

OCPI_SOURCE_PATH_FRAME = {"name": "Ocpi_Setup_Configuration", "type": "OcpiSourceDialog"}
SOURCE_EXPLORER_BUTTON = {"name": "toolButton", "type": "QToolButton", 
    "window": OCPI_SOURCE_PATH_FRAME}
FILE_EXPLORER_FRAME = {"name": "QFileDialog", "type": "QFileDialog"}
DIR_LIST = { "name": "treeView", "type": "QTreeView"}
OK_BUTTON = {"text": "OK", "type": "QPushButton",  "window": OCPI_SOURCE_PATH_FRAME}
OCPI_PROJECT_PATH_FRAME = {"name": "Project_Path_Dialog", "type": "ProjectExplorerPath"}
PROJECT_EXPLORER_BUTTON = {"name": "toolButton", "type": "QToolButton", 
    "window": OCPI_PROJECT_PATH_FRAME}
PROJECT_CHOOSE_BUTTON = {"text": "Choose", "type": "QPushButton", "window": FILE_EXPLORER_FRAME}
EXPLORER_OK_BUTTON = {"text": "OK", "type": "QPushButton", "window": OCPI_PROJECT_PATH_FRAME}
DISPLAYED_PROJECT_PATH = {"name": "Current_PE_Path_Output_label", "type": "QLabel", "visible": 1}
DISPLAYED_SOURCE_PATH = {"name": "Current_opencpi_Dir_label", "type": "QLabel"}
MAIN_WINDOW = {"name": "OpenCPI", "type": "MainWindow", "windowTitle": "OpenCPI GUI"}
    
def set_source_path_initial():
    """Sets the source path on initial startup.
    
    When launching ocpigui for first time, source path needs to be set.
    """
    test.log('[Init Env Paths] Set source path on start up')
    test.compare(squish.waitForObjectExists(OCPI_SOURCE_PATH_FRAME).visible, True)
    squish.clickButton(squish.waitForObject(SOURCE_EXPLORER_BUTTON))
    test.compare(squish.waitForObjectExists(FILE_EXPLORER_FRAME).visible, True)
    squish.mouseClick(squish.waitForObjectItem(DIR_LIST, 'opencpi'))
    ocpi.click_button(PROJECT_CHOOSE_BUTTON, 'Choose')
    ocpi.click_button(OK_BUTTON, 'OK')

def set_project_path_initial():
    """Sets project path when the GUI is started each time.
    
    Project path is reguired on each launch of ocpigui.
    """
    test.log('[Init Env Paths] Set project path on start up')
    test.compare(squish.waitForObjectExists(OCPI_PROJECT_PATH_FRAME).visible, True)
    squish.clickButton(squish.waitForObject(PROJECT_EXPLORER_BUTTON))
    test.compare(squish.waitForObjectExists(FILE_EXPLORER_FRAME).visible, True)
    squish.mouseClick(squish.waitForObjectItem(DIR_LIST, 'User\\_OpenCPI\\_Projects'))
    ocpi.click_button(PROJECT_CHOOSE_BUTTON, 'Choose')
    ocpi.click_button(EXPLORER_OK_BUTTON, 'OK')
    
def verify_paths(proj_path, source_path):
    """Verify the source/project paths are correct.

    Args:
        proj_path: Path for the project explorer frame in gui.
        source_path: Path for the ocpi environment source path.
    """
    test.log('[Init Env Paths] Verify Source/Project Path Displayed Correctly')
    squish.waitForObject(MAIN_WINDOW)
    project_path = proj_path
    test.compare(str(squish.waitForObjectExists(DISPLAYED_PROJECT_PATH).text), str(project_path))
    ocpi_source_path = source_path
    test.compare(str(squish.waitForObjectExists(DISPLAYED_SOURCE_PATH).text), str(ocpi_source_path))
    
def set_new_source_path(dir_name):
    """Sets the source path from within the GUI.
     
    Args:
        dir_name: Directory name for project path
    """
    test.log('[Init Env Paths] Set source path within GUI')
    test.compare(squish.waitForObjectExists(OCPI_SOURCE_PATH_FRAME).visible, True)
    squish.clickButton(squish.waitForObject(SOURCE_EXPLORER_BUTTON))
    test.compare(squish.waitForObjectExists(FILE_EXPLORER_FRAME).visible, True)
    squish.mouseClick(squish.waitForObjectItem(DIR_LIST, dir_name))
    squish.clickButton(squish.waitForObject(PROJECT_CHOOSE_BUTTON))
    squish.clickButton(squish.waitForObject(OK_BUTTON))
    
def set_new_project_path(dir_name):
    """Sets the project path from within the GUI.
     
    Args:
        dir_name: Directory name for project path
    """
    test.log('[Init Env Paths] Set project path within GUI')
    test.compare(squish.waitForObjectExists(OCPI_PROJECT_PATH_FRAME).visible, True)
    squish.clickButton(squish.waitForObject(PROJECT_EXPLORER_BUTTON))
    test.compare(squish.waitForObjectExists(FILE_EXPLORER_FRAME).visible, True)
    squish.doubleClick(squish.waitForObjectItem(DIR_LIST, dir_name))
    squish.clickButton(squish.waitForObject(PROJECT_CHOOSE_BUTTON))
    squish.clickButton(squish.waitForObject(EXPLORER_OK_BUTTON))