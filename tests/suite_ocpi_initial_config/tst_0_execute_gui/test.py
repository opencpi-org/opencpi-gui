# -*- coding: utf-8 -*-
from pageobjects import init_env_paths
from pathlib import Path
from os import environ

def main():
    """Runs the ocpi gui for the first time.

    Sets project path and source path if needed.
    """
    # Grab ocpi GUI path since source path dialog will appear.
    ocpi = str(Path(environ['OCPI_ROOT_DIR'], 'exports', environ['OCPI_TOOL_PLATFORM'], 
         'bin', 'ocpigui'))
    startApplication(ocpi)
    
    # Checking to see if object exists.
    # Because source path dialog displays on first launch of GUI. 
    if (object.exists(init_env_paths.OCPI_SOURCE_PATH_FRAME)):
        init_env_paths.set_source_path_initial()
    
    init_env_paths.set_project_path_initial()