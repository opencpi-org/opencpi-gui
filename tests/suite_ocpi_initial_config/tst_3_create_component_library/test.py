# -*- coding: utf-8 -*-
import ocpi_gui
from pageobjects import ocpi, create_component_library
from pathlib import Path 
from os import environ

def main():
    """Will create a component library. 
    
    Verifies that all files were generated.
    Confirms that the correct ocpidev command was ran.
    """
    ocpi_gui.start()
    
    test.startSection('New Component Library Cancel')
    ocpi.open_component_library()
    test.compare(waitForObjectExists(create_component_library.FRAME).visible, True, 
        'Verify New Component Library Window Displays')
    # Confirming that the Cancel button works here.
    ocpi.cancel(ocpi.LIB_CANCEL_BUTTON)
    snooze(2) 
    # Snooze for 2 seconds before confirming that the new component library window is closed.
    test.compare(object.exists(create_component_library.FRAME), False, 
        'Verify New Component Library Window Closes')
    test.endSection()
    
    test.startSection('New Component Library OK')
    ocpi.open_component_library()
    test.compare(waitForObjectExists(create_component_library.FRAME).visible, True, 
        'Verify New Component Library Window Displays')
    create_component_library.enter()
    ocpi.confirm(ocpi.LIB_OK_BUTTON)
    test.endSection()
    
    test.startSection('Verify Component Library Created')
    snooze(2)
    # Confirm that the components library was generated after snoozing for 2 seconds.
    comps_dir = Path(environ['HOME'], 'User_OpenCPI_Projects', 'Demo', 'components')
    test.verify(comps_dir.exists(), 'Component Library Exist')
    test.endSection()
    
    test.startSection('Verify ocpidev Command is Correct')
    # Check to see if the ocpidev command in gui matches cli version.
    project_path = str(Path(environ['HOME'], 'User_OpenCPI_Projects', 'Demo'))
    command = f'COMMAND RAN: ocpidev create library components -d {project_path} '
    gui_command = str(waitForObjectExists(ocpi.CONSOLE_LOG).plainText)
    
    # Check if command is in the Console Log, since it contains more than just command ran.
    if command in gui_command:
        test.passes('Correct ocpidev Command Ran')
    else:
        test.fail('Incorrect ocpidev Command Ran')
    test.endSection()
    