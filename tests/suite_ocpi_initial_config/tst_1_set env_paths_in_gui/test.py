# -*- coding: utf-8 -*-
import ocpi_gui
import test 
from pageobjects import init_env_paths, ocpi
from pathlib import Path
from os import environ 

def main():
    """Sets the source/environment paths from within the GUI.

    Then verify that the correct paths were set.
    """
    ocpi_gui.start()
    test.compare(waitForObjectExists(ocpi.MAIN_WINDOW).visible, True, 'Verify OCPI GUI visible')

    test.startSection('Enter New Project Path')
    ocpi.open_project_path_dialog()
    # Setting project explorer path to User_OpenCPI_Projects.
    init_env_paths.set_new_project_path('User\\_OpenCPI\\_Projects')
    test.endSection()
    
    test.startSection('Enter New Source Path')
    ocpi.open_source_path_dialog()
    # Setting ocpi source path to root directory of opencpi.
    init_env_paths.set_new_source_path('opencpi')
    test.endSection()
    
    test.startSection('Verify Source/Project Path Updated')
    # Verify the newly set paths are correct.
    proj_path = (Path(environ['HOME'], 'User_OpenCPI_Projects'))
    source_path = str(Path(environ['OCPI_ROOT_DIR']))
    # Passes expected project and source path to verify they are same.
    init_env_paths.verify_paths(proj_path, source_path)
    test.endSection()