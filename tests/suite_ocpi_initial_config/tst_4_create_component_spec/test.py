# -*- coding: utf-8 -*-
import ocpi_gui
from pageobjects import ocpi, create_component_spec
from pathlib import Path
from os import environ

def main():
    """Creates a component spec.
    
    Confirms that all correct files are generated.
    Checks that the correct ocpidev command was ran.
    """
    ocpi_gui.start()
    
    test.startSection('New Component Spec Cancel')
    ocpi.open_component_spec()
    test.compare(waitForObjectExists(create_component_spec.FRAME).visible, True, 
         'Verify New Component Spec Window Displays')
    # Confirming that the cancel button works here
    ocpi.cancel(ocpi.COMP_CANCEL_BUTTON)
    # Snooze for 2 seconds before confirming that the new component spec window is closed.
    snooze(2)
    test.compare(object.exists(create_component_spec.FRAME), False, 
        'Verify New Component Library Window Closes')
    test.endSection()
    
    test.startSection('New Component Spec OK')
    ocpi.open_component_spec()
    test.compare(waitForObjectExists(create_component_spec.FRAME).visible, True, 
        'Verify New Component Spec Window Displays')
    create_component_spec.enter('test', 'Demo')
    ocpi.confirm(ocpi.COMP_OK_BUTTON)
    snooze(2)
    test.endSection()
    
    test.startSection('Verify Component Spec Created')
    snooze(2)
    # Confirm that the components spec dir/files were generated after snoozing for 2 seconds.
    dataset = testData.dataset('spec_files.tsv')
    for row in dataset:
        path = testData.field(row, '1')
        full_path = Path(environ['HOME'], path)
        test.verify(full_path.exists(), path + ' exists' )
    test.endSection()
        
    test.startSection('Verify ocpidev Command is Correct')
    # Check to see if the ocpidev command in gui matches cli version.
    # 
    project_path = str(Path(environ['HOME'], 'User_OpenCPI_Projects', 'Demo'))
    command = f'COMMAND RAN: ocpidev create component test -d {project_path} -l components '
    gui_command = str(waitForObjectExists(ocpi.CONSOLE_LOG).plainText)
    
    # Check if command is in the Console Log, since it contains more than just command ran.
    if command in gui_command:
        test.passes('Correct ocpidev Command Ran')
    else:
        test.fail('Incorrect ocpidev command ran')
    test.endSection()
