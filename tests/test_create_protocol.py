from PyQt5.QtCore import Qt

import OpenCPI_GUI


class TestCompSpecDialog:
    def test_app_name(self, qtbot):
        window = OpenCPI_GUI.NewProtocolDialog()
        window.show()
        qtbot.addWidget(window)

        window.Protocol_Name_lineEdit.clear()
        qtbot.keyClicks(window.Protocol_Name_lineEdit, "dummy_spec")
        qtbot.keyClicks(window.protocol_project, "dummy_proj")
        qtbot.keyClicks(window.protocol_library, "dummy_lib")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.Protocol_Name_lineEdit.text() == "dummy_spec"
        assert window.Project_comboBox.currentText() == "dummy_proj"
        assert window.Library_comboBox.currentText() == "dummy_lib"
