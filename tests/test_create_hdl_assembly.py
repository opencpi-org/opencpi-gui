from PyQt5.QtCore import Qt

import OpenCPI_GUI

class TestHDLAssembly:
    def test_assembly_name(self, qtbot):
        window = OpenCPI_GUI.NewHdlAssemblyDialog()
        window.show()
        qtbot.addWidget(window)

        window.HDL_Assem_Name_lineEdit.clear()
        qtbot.keyClicks(window.HDL_Assem_Name_lineEdit, "dummy_assembly")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.HDL_Assem_Name_lineEdit.text() == "dummy_assembly"

    def test_assembly_project(self, qtbot):
        window = OpenCPI_GUI.NewHdlAssemblyDialog()
        window.show()
        qtbot.addWidget(window)

        window.HDL_Assem_Name_lineEdit.clear()
        qtbot.keyClicks(window.HDL_Assem_Name_lineEdit, "dummy_assembly")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.HDL_Assem_Name_lineEdit.text() == "dummy_assembly"
        assert window.Project_comboBox.currentText() == "dummy_proj"