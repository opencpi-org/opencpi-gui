from PyQt5.QtCore import Qt

import OpenCPI_GUI

class TestPrimCore:
    def test_prim_core_name(self, qtbot):
        window = OpenCPI_GUI.NewHdlPrimCoreDialog()
        window.show()
        qtbot.addWidget(window)

        window.HDL_Prim_Core_Name_lineEdit.clear()
        qtbot.keyClicks(window.HDL_Prim_Core_Name_lineEdit, "dummy_prim_core")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.HDL_Prim_Core_Name_lineEdit.text() == "dummy_prim_core"


    def test_prim_core_project(self, qtbot):
        window = OpenCPI_GUI.NewHdlPrimCoreDialog()
        window.show()
        qtbot.addWidget(window)

        window.HDL_Prim_Core_Name_lineEdit.clear()
        qtbot.keyClicks(window.HDL_Prim_Core_Name_lineEdit, "dummy_prim_core")
        qtbot.keyClicks(window.Project_comboBox, "dummy_proj")
        qtbot.mouseClick(window.buttonBox, Qt.LeftButton)

        assert window.HDL_Prim_Core_Name_lineEdit.text() == "dummy_prim_core"
        assert window.Project_comboBox.currentText() == "dummy_proj"