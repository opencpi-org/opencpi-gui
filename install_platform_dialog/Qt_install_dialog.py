# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'install_platform_layout.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Install_Platform_Dialog(object):
    def setupUi(self, Install_Platform_Dialog):
        Install_Platform_Dialog.setObjectName("Install_Platform_Dialog")
        Install_Platform_Dialog.resize(400, 265)
        self.verticalLayout = QtWidgets.QVBoxLayout(Install_Platform_Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Install_Platform_Dialog)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.Platform_Name_lineEdit = QtWidgets.QLineEdit(Install_Platform_Dialog)
        self.Platform_Name_lineEdit.setObjectName("Platform_Name_lineEdit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.Platform_Name_lineEdit)
        self.label_2 = QtWidgets.QLabel(Install_Platform_Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.Package_ID_lineEdit = QtWidgets.QLineEdit(Install_Platform_Dialog)
        self.Package_ID_lineEdit.setObjectName("Package_ID_lineEdit")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.Package_ID_lineEdit)
        self.label_3 = QtWidgets.QLabel(Install_Platform_Dialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.URL_lineEdit = QtWidgets.QLineEdit(Install_Platform_Dialog)
        self.URL_lineEdit.setObjectName("URL_lineEdit")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.URL_lineEdit)
        self.label_4 = QtWidgets.QLabel(Install_Platform_Dialog)
        self.label_4.setWordWrap(True)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.Git_Rev_lineEdit = QtWidgets.QLineEdit(Install_Platform_Dialog)
        self.Git_Rev_lineEdit.setObjectName("Git_Rev_lineEdit")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.Git_Rev_lineEdit)
        self.minimal_install_checkBox = QtWidgets.QCheckBox(Install_Platform_Dialog)
        self.minimal_install_checkBox.setObjectName("minimal_install_checkBox")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.minimal_install_checkBox)
        self.verticalLayout.addLayout(self.formLayout)
        self.label_5 = QtWidgets.QLabel(Install_Platform_Dialog)
        self.label_5.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_5.setWordWrap(True)
        self.label_5.setObjectName("label_5")
        self.verticalLayout.addWidget(self.label_5)
        self.buttonBox = QtWidgets.QDialogButtonBox(Install_Platform_Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Install_Platform_Dialog)
        self.buttonBox.accepted.connect(Install_Platform_Dialog.accept)
        self.buttonBox.rejected.connect(Install_Platform_Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Install_Platform_Dialog)

    def retranslateUi(self, Install_Platform_Dialog):
        _translate = QtCore.QCoreApplication.translate
        Install_Platform_Dialog.setWindowTitle(_translate("Install_Platform_Dialog", "Install Platform"))
        self.label.setText(_translate("Install_Platform_Dialog", "Platform Name"))
        self.label_2.setText(_translate("Install_Platform_Dialog", "Package ID"))
        self.Package_ID_lineEdit.setPlaceholderText(_translate("Install_Platform_Dialog", "[Optional]"))
        self.label_3.setText(_translate("Install_Platform_Dialog", "URL"))
        self.URL_lineEdit.setPlaceholderText(_translate("Install_Platform_Dialog", "[Optional with Package ID]"))
        self.label_4.setText(_translate("Install_Platform_Dialog", "Git Revision"))
        self.Git_Rev_lineEdit.setPlaceholderText(_translate("Install_Platform_Dialog", "[Optional with Package ID]"))
        self.minimal_install_checkBox.setText(_translate("Install_Platform_Dialog", "Minimal Install"))
        self.label_5.setText(_translate("Install_Platform_Dialog", "If the Package ID is provided, the URL and Git revision are not necessary. If not provided, the package ID can be extracted from the downloaded file."))

